/*************************************************************************
 * sort.h - from bibtex2html distribution
 *
 * $Id: sort.h,v 2.3 2004/05/19 14:02:25 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */


#ifndef _bibtex2hml_sort_
#define _bibtex2hml_sort_


#ifdef __cplusplus
extern "C" {
#endif



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef WIN32
#define strcasecmp  strcmpi
#define strncasecmp strnicmp
#else
extern int strcasecmp(const char *s1, const char *s2);
#endif

#include <biblio.h>

extern int compare_key_names( char *key1, char *key2 );
extern void sort_names( typeName *name, int left, int right );

extern void set_complete_equality_of_names();
extern void set_partial_equality_of_names();

extern void update_keywords( typeListOfItems *li, typeListOfNames *ln, 
		      typeListOfNames *list_of_indexed_names );
extern void update_names( typeListOfItems *li, typeListOfNames *ln, 
			  typeListOfNames *list_of_indexed_names );

extern void sort_list_of_items( typeListOfItems *l, enumSortCriterium c );
extern void sort_list_of_items_with_multiple_criteria( typeListOfItems *l, 
						       enumSortCriterium c[SORT_CRITERIA_NB] );

#ifdef __cplusplus
}
#endif

#endif
