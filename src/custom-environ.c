/*************************************************************************
 * custom-environ.c - from bibtex2html distribution
 *
 * $Id: custom-environ.c,v 2.5 2004/07/16 17:33:55 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */


#include <custom-environ.h>

/* some global constant
   for predefined styles
 */

char *default_environment_description[] =
{ "@STRING{ directory_authors = \"Author\" }",
  "@STRING{ directory_biblio = \"Biblio\" }",
  "@STRING{ directory_categories = \"Category\" }",
  "@STRING{ directory_keywords = \"Keyword\" }",
  "@STRING{ directory_years = \"Year\" }",
  "@STRING{ directory_icons = \"Icons\" }",
  "@STRING{ filename_complete_biblio = \"complete-bibliography\" }",
  "@STRING{ filename_index = \"index\" }",
  "@STRING{ filename_extension = \"html\" }",
  "XXX"
};



char *default_environment_description_html[] =
{ "@STRING{ filename_extension = \"html\" }",
  "@STRING{ use_html_links_between_pages = 1 }",
  "@STRING{ use_html_links_towards_urls = 1 }",
  "XXX"
};



char *default_environment_description_bibtex[] =
{ "@STRING{ filename_extension = \"bib\" }",
  "@STRING{ use_html_links_between_pages = 0 }",
  "@STRING{ use_html_links_towards_urls = 0 }",
  "XXX"
};



char *default_environment_description_latex[] =
{ "@STRING{ filename_extension = \"tex\" }",
  "@STRING{ use_html_links_between_pages = 0 }",
  "@STRING{ use_html_links_towards_urls = 0 }",
  "@STRING{ html_link_tag.start =\"\n\\verb|\" }",
  "@STRING{ html_link_tag.end =\"| \" }",
  "XXX"
};



char *default_environment_description_txt[] =
{ "@STRING{ filename_extension = \"txt\" }",
  "@STRING{ use_html_links_between_pages = 0 }",
  "@STRING{ use_html_links_towards_urls = 0 }",
  "XXX"
};



char *default_environment_description_none[] = 
{ "@STRING{ filename_extension = \"none\" }",
  "@STRING{ use_html_links_between_pages = 0 }",
  "@STRING{ use_html_links_towards_urls = 0 }",
  "XXX"
};





/* description of the structure
 */
typeArgDescription environment_description[] = 
{ { "directory_authors",        _STRING_, NULL },
  { "directory_biblio",         _STRING_, NULL },
  { "directory_categories",     _STRING_, NULL },
  { "directory_keywords",       _STRING_, NULL },
  { "directory_years",          _STRING_, NULL },
  { "directory_icons",          _STRING_, NULL },
  { "iconsdir",                 _STRING_, NULL },
  { "filename_complete_biblio", _STRING_, NULL },
  { "filename_index",           _STRING_, NULL },
  { "filename_extension",       _STRING_, NULL },
  { "use_html_links_between_pages", _INT_,  NULL },
  { "use_html_links_towards_urls",  _INT_,  NULL },
  { "use_icons",                _INT_,  NULL },
  { "XXX",                      _STRING_, NULL }
}; 










void init_environment_description( typeEnvironmentDescription *e )
{
  e->directory_authors = NULL;
  e->directory_biblio = NULL;
  e->directory_categories = NULL;
  e->directory_keywords = NULL;
  e->directory_years = NULL;

  e->directory_icons = NULL;

  e->iconsdir = NULL;

  e->filename_complete_biblio = NULL;
  e->filename_index = NULL;
  e->filename_extension = NULL;

  e->use_html_links_between_pages = 0;
  e->use_html_links_towards_authors_pages = 0;
  e->use_html_links_towards_categories_pages = 0;
  e->use_html_links_towards_reduced_years_pages = 0;
  e->use_html_links_towards_complete_years_pages = 0;
  e->use_html_links_towards_keywords_pages = 0;

  e->use_html_links_towards_urls = 0;

  e->use_icons = 0;
}



void free_environment_description( typeEnvironmentDescription *e )
{
  if ( e->directory_authors != NULL ) FREE( e->directory_authors, "free_environment_description" );
  if ( e->directory_biblio != NULL ) FREE( e->directory_biblio, "free_environment_description" );
  if ( e->directory_categories != NULL ) FREE( e->directory_categories, "free_environment_description" );
  if ( e->directory_keywords != NULL ) FREE( e->directory_keywords, "free_environment_description" );
  if ( e->directory_years != NULL ) FREE( e->directory_years, "free_environment_description" );

  if ( e->directory_icons != NULL ) FREE( e->directory_icons, "free_environment_description" );

  if ( e->iconsdir != NULL ) FREE( e->iconsdir, "free_environment_description" );

  if ( e->filename_complete_biblio != NULL ) FREE( e->filename_complete_biblio, "free_environment_description" );
  if ( e->filename_index != NULL ) FREE( e->filename_index, "free_environment_description" );
  if ( e->filename_extension != NULL ) FREE( e->filename_extension, "free_environment_description" );

  init_environment_description( e );
}



/* add environment strings to customization strings
 */
int add_environment_strings( typeArgDescription *custom,
			     int length,
			     typeEnvironmentDescription *e,
			     typeArgDescription *ed )
{
  char *proc = "add_environment_strings";
  int l = length;
  int i, j, n;

  for ( i = 0, j = 0; strncmp( ed[i].key, "XXX", 3 ) != 0; i ++, j += n ) {

    n = 0;

    switch( ed[i].type ) {
      
    default :
      fprintf( stderr, "%s: unknown arg type\n", proc );
      break;

    case _STRING_ :

      custom[l+j]     = ed[i];
      if ( strcmp( ed[i].key, "directory_authors" ) == 0 )
	custom[l+j].arg = &(e->directory_authors);
      else if ( strcmp( ed[i].key, "directory_biblio" ) == 0 )
	custom[l+j].arg = &(e->directory_biblio);
      else if ( strcmp( ed[i].key, "directory_categories" ) == 0 )
	custom[l+j].arg = &(e->directory_categories);
      else if ( strcmp( ed[i].key, "directory_keywords" ) == 0 )
	custom[l+j].arg = &(e->directory_keywords);
      else if ( strcmp( ed[i].key, "directory_years" ) == 0 )
	custom[l+j].arg = &(e->directory_years);
      else if ( strcmp( ed[i].key, "directory_icons" ) == 0 )
	custom[l+j].arg = &(e->directory_icons);
      else if ( strcmp( ed[i].key, "iconsdir" ) == 0 )
	custom[l+j].arg = &(e->iconsdir);
      else if ( strcmp( ed[i].key, "filename_complete_biblio" ) == 0 )
	custom[l+j].arg = &(e->filename_complete_biblio);
      else if ( strcmp( ed[i].key, "filename_index" ) == 0 )
	custom[l+j].arg = &(e->filename_index);
      else if ( strcmp( ed[i].key, "filename_extension" ) == 0 )
	custom[l+j].arg = &(e->filename_extension);
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, ed[i].key );
      }     
      n = 1;
      break;

    case _INT_ :

      custom[l+j]     = ed[i];
      if ( strcmp( ed[i].key, "use_html_links_between_pages" ) == 0 )
	custom[l+j].arg = &(e->use_html_links_between_pages);
      else if ( strcmp( ed[i].key, "use_html_links_towards_urls" ) == 0 )
	custom[l+j].arg = &(e->use_html_links_towards_urls);
      else if ( strcmp( ed[i].key, "use_icons" ) == 0 )
	custom[l+j].arg = &(e->use_icons);
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, ed[i].key );
      }     
      n = 1;
      break;
      
    }
  }
  return ( l+j );
}
