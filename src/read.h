/*************************************************************************
 * read.h - from bibtex2html distribution
 *
 * $Id: read.h,v 2.9 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2001, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#ifndef _read_h_
#define _read_h_

#ifdef __cplusplus
extern "C" {
#endif



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int strcasecmp(const char *s1, const char *s2);
extern int strncasecmp(const char *s1, const char *s2, size_t n);

#include <common.h>

#include <biblio.h>
#include <sort.h>
#include <custom.h>


/************************************************************
 * MISC
 ************************************************************/
extern int count_words( char *str );

/************************************************************
 * NAMES MANAGEMENT
 ************************************************************/
extern void init_list_of_names          ( typeListOfNames *li );
extern void release_list_of_names       ( typeListOfNames *li );
extern void print_list_of_names         ( FILE *file, typeListOfNames *li );
extern  int retrieve_names_to_be_indexed( typeListOfNames *ln, 
					  typeListOfNames *list_of_indexed_names, 
					  typeListOfNames *list_of_equivalent_names );

/************************************************************
 * STRINGS MANAGEMENT
 ************************************************************/
extern void init_list_of_strings   ( typeListOfStrings *ls );
extern void release_list_of_strings( typeListOfStrings *ls );
extern void print_list_of_strings( typeListOfStrings *ls );

/************************************************************
 * BIBLIOGRAPHIC ITEM MANAGEMENT
 ************************************************************/
extern void print_item           ( FILE *file, typeItem *item );
extern void init_list_of_items   ( typeListOfItems *li );
extern void release_list_of_items( typeListOfItems *li );

/************************************************************
 * BIBLIOGRAPHY MANAGEMENT
 ************************************************************/
extern int retrieve_all_names( typeListOfNames *ln, typeListOfItems *li );
extern int retrieve_all_keywords( typeListOfNames *ln, typeListOfItems *li );

/************************************************************
 * BIBTEX  READING
 ************************************************************/
extern int ReadBibtexInputs( typeListOfItems *list_of_items, 
			     typeListOfStrings *list_of_strings,
			     typeListOfNames *list_of_indexed_names, 
			     typeListOfNames *list_of_equivalent_names, 
			     typeListOfNames *list_of_indexed_keywords,
			     typeListOfNames *list_of_equivalent_keywords,
			     typeArgDescription *customization_strings,
			     char *filename );
extern int CountBibtexInputs( char *filename );

/************************************************************
 * MEDLINE READING
 ************************************************************/
extern int ReadMedlineInputs( typeListOfItems *list_of_items, 
			      typeListOfStrings *list_of_strings,
			      typeListOfNames *list_of_indexed_names, 
			      typeListOfNames *list_of_equivalent_names, 
			      typeListOfNames *list_of_indexed_keywords,
			      typeListOfNames *list_of_equivalent_keywords,
			      typeArgDescription *customization_strings,
			      char *filename );

/************************************************************
 * CONTEXT STUFF
 ************************************************************/
extern void print_context();
extern void load_predefined_strings( typeArgDescription *customization_strings, 
			      char **str );
extern void load_predefined_style( typeArgDescription *customization_strings,
			    enumPredefinedStyle style );

/************************************************************
 * MISC MANAGEMENT
 ************************************************************/
extern void set_verbose_in_read();
extern void unset_verbose_in_read();
extern void set_warning_in_read();
extern void unset_warning_in_read();
extern void set_debug_in_read();
extern void unset_debug_in_read();

#ifdef __cplusplus
}
#endif

#endif
