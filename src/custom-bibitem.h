/*************************************************************************
 * custom-bibitem.h - from bibtex2html distribution
 *
 * $Id: custom-bibitem.h,v 2.7 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */



#ifndef _custom_bibitem_h_
#define _custom_bibitem_h_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>

#include <common.h>

#include <custom-common.h>



/* what is needed to write a bibitem
 */

typedef struct {

  typeTagElement bib_firstname;
  typeTagElement bib_lastname;
  typeTagElement bib_booktitle;
  typeTagElement bib_journal;
  typeTagElement bib_series;
  typeTagElement bib_title;

  typeEnvElement bib_note;

  typeEnvElement bib_doi;
  typeEnvElement bib_pdf;
  typeEnvElement bib_pmid;
  typeEnvElement bib_postscript;
  typeEnvElement bib_url;
  typeEnvElement bib_url_hal;

  typeEnvElement bib_abstract;
  typeEnvElement bib_annote;
  typeEnvElement bib_bibtex_entry;
  typeEnvElement bib_bibtex_file;
  typeEnvElement bib_bibtex_key;
  typeEnvElement bib_comments;
  typeEnvElement bib_hal_identifiant;
  typeEnvElement bib_isbn;
  typeEnvElement bib_issn;
  typeEnvElement bib_keywords;

  char *bib_comma;
  char *bib_dot;

  char *bib_and;

} typeBibtexItemDescription;



/* global constants
 */
extern char *default_bibitem_description[];

extern char *default_bibitem_description_html[];
extern char *default_bibitem_description_latex[];
extern char *default_bibitem_description_txt[];
extern char *default_bibitem_description_none[];


extern typeArgDescription bibitem_description[];


/* prototypes
 */
extern void init_bibtex_item_description( typeBibtexItemDescription *b );
extern void free_bibtex_item_description( typeBibtexItemDescription *b );
extern int  add_bibitem_strings( typeArgDescription *custom,
				 int length,
				 typeBibtexItemDescription *b,
				 typeArgDescription *bd );

#ifdef __cplusplus
}
#endif

#endif
