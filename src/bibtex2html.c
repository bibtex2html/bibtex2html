/*************************************************************************
 * bib2html.c - from bib2html distribution
 *
 * $Id: bibtex2html.c,v 2.23 2004/07/20 16:38:19 greg Exp $
 *
 * CopyrightŠINRIA 2001
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */

#include <stdio.h>

#include <common.h>

#include <read.h>
#include <sort.h>
#include <write.h>
#include <custom.h>


static int _verbose_ = 1;



static char *version = "version 1.03";

static char *options = "\
 [-bibtex]\
 [-check-icons]\
 [-complete-equality-of-names | -ceon]\
 [-copy-icons]\
\n\
 [-create-directories | -force]\
 [-help | --help]\
\n\
 [-html-links | -use-html-links]\
 [-icons | -use-icons]\
 [-iconsdir %s]\
\n\
 [-medline]\
 [-no-html-links]\
 [-no-icons]\
 [-no-verbose | -nv]\
 [-no-warning | -nw]\
\n\
 [-partial-equality-of-names | -peon]\
\n\
 [-print-bibtex-all-fields]\
 [-print-bibtex-fields]\
\n\
 [-print-bibtex-full-templates]\
 [-print-bibtex-templates]\
 [-print-context]\
\n\
 [-print-customization-keys | -print-customisation-keys | -print-keys]\
\n\
 [-print-default-context]\
 [-print-full-context]\
 [-print-keywords]\
\n\
 [-print-file %s]\
 [-print-names]\
 [-single-output | -output %s]\
\n\
 [-sort-criterium | -sort {cat | year | name  | type | bibkey}]\
 [-stderr | -stdout]\
\n\
 [-style {no-style | bibtex | html | latex | layout | txt}]\
\n\
 [-version]\
 [-verbose | -v]\
 [-warning | -w]\
\n\
\t bibliography files ... \n";





static char *details = "\
 [-bibtex]\
\n\
\t from now on, input files are considered to be BibTeX files.\
\n\
 [-check-icons]\
\n\
 [-complete-equality-of-names | -ceon]\
\n\
\t names are supposed to be the same if both last names\n\
\t are equal, if the initials of the first names are the\n\
\t same, and there are the same number of initials.\n\
\t Eg, J. Bach and J.S. Bach do not represent the same name,\n\
\t but J.S. Bach and J.-S. Bach do.\
\n\
 [-copy-icons]\
\n\
 [-create-directories | -force]\
\n\
 [-help | --help]\
\n\
 [-html-links | -use-html-links]\
\n\
\t use html links between multiple pages.\
\n\
 [-icons | -use-icons]\
\n\
\t use icons (if any) for links towards www, pdf, doi or postcript urls\
\n\
 [-iconsdir %s]\
\n\
\t directory from where the icons are to be copied. Overrides all other\n\
\t directories specifications.\n\
\n\
 [-medline]\
\n\
\t from now on, input files are considered to be MedLine files.\
\n\
 [-no-html-links]\
\n\
 [-no-icons]\
\n\
 [-no-verbose | -nv]\
\n\
 [-no-warning | -nw]\
\n\
 [-partial-equality-of-names | -peon]\
\n\
\t names are supposed to be the same if both last names are equal\n\
\t and if the initials of the first names are the same. \n\
\t Eg, J. Bach and J.S. Bach represent the same name.\
\n\
 [-print-bibtex-all-fields]\
\n\
 [-print-bibtex-fields]\
\n\
 [-print-bibtex-full-templates]\
\n\
 [-print-bibtex-templates]\
\n\
 [-print-context]\
\n\
\t print 'context' strings (ie customization keys and the associated value:\n\
\t see documentation), after modifications given by input files, if any.\n\
\t Strings without values are not printed out by default:\n\
\t use '-print-full-context' to print them all.\n\
\t Do not generate any output.\
\n\
 [-print-customization-keys | -print-customisation-keys | -print-keys]\
\n\
\t print all the available customization keys. They are \n\
\t to be used in '@STRING{ customization-key = \" my will \" };'\n\
\t see documentation for details.\n\
\t Use '-print-context' to see both the keys and their values.\n\
\t Do not generate any output.\
\n\
 [-print-default-context]\
\n\
\t print 'context' strings (ie customization keys and the associated value:\n\
\t see documentation) with their default values, ie before modifications\n\
\t \t Strings without values are not printed out by default:\n\
\t use '-print-full-context' to print them all.\n\
\t Do not generate any output.\
\n\
 [-print-file %s]\
\n\
\t Specify an output file for the options '-print-context',\n\
\t '-print-customization-keys', '-print-default-context',\n\
\t '-print-full-context', '-print-keywords', '-print-names'\n\
\t and '-print-templates'.\n\
\t Default is 'stdout'.\
\n\
 [-print-full-context]\
\n\
\t Allows to print empty 'context' strings when use in conjonction with\n\
\t either '-print-context' or '-print-default-context'. If specified alone,\n\
\t is equivalent to '-print-context -print-full-context'.\
\n\
\t Idem -print-context, except that strings without values are also\
\t printed out.\n\
\n\
 [-print-keywords]\
\n\
\t Print all the founds keywords, and the keywords that will be indexed.\n\
\t Output is generated.\
\n\
 [-print-names]\
\n\
\t Print all the founds authors' names, and the names that will be indexed.\n\
\t Output is generated.\
\n\
 [-single-output | -output %s]\
\n\
\t write into a single file (stdout if %s == '-')\n\
\t one may find this option useful to merge several files into one single\n\
\t file, to use in conjonction with the -sort-criterium and -style options\n\
\t ex: the command `bibtex2html *.bib -sort names -style bibtex -output foo.bib`\n\
\t will merge the bibtex files into 'foo.bib', the sorting ensuring that double\n\
\t entries will follow each other.\
\n\
 [-sort-criterium | -sort {cat | year | name | type | bibkey}]\
\n\
 [-stderr | -stdout]\
\n\
\t write the message on the specified stream\
\n\
 [-style {no-style | bibtex | html | latex | layout | txt}]\
\n\
\t writing style of output\
\n\
 [-verbose | -v]\
\n\
\t print messages if any\
\n\
 [-warning | -w]\
\n\
\t print (in stderr) warnings messages if any\
\n\
\n\
 $Revision: 2.23 $ $Date: 2004/07/20 16:38:19 $ $Author: greg $\n";



static void usage( char *prg, char *str, int flag )
{
  fprintf( stderr, "\n" );
  fprintf( stderr, "%s:\n%s\n", prg, options );
  if ( flag ) 
    fprintf( stderr, "%s\n", details );
  if ( str != NULL ) 
    fprintf( stderr, "Error: %s\n", str );
}









int main( int argc, char *argv[], char *arge[] )
{
  /* what is needed for customization
   */
  typeEnvironmentDescription  desc_environment;
  typeGenericFileDescription  desc_generic_file;
  typeSpecificFileDescription desc_single_file;
  typeSpecificFileDescription desc_index_file;
  typeSpecificFileDescription desc_author_file;
  typeSpecificFileDescription desc_category_file;
  typeSpecificFileDescription desc_keyword_file;
  typeSpecificFileDescription desc_reduced_year_file;
  typeSpecificFileDescription desc_complete_year_file;
  typeSpecificFileDescription desc_complete_biblio_file;
  typeSpecificFileDescription desc_default_file;
  typeBibtexItemDescription   desc_bibitem;

  typeArgDescription *customization_strings = NULL;
  enumPredefinedStyle load_style = _HTML_;

  int is_context_to_be_printed = 0;
  int is_default_context_to_be_printed = 0;
  int is_full_context_to_be_printed = 0;
  
  int are_customization_keys_to_be_printed = 0;

  int are_bibtex_fields_to_be_printed = 0;
  int are_bibtex_all_fields_to_be_printed = 0;
  int are_bibtex_templates_to_be_printed = 0;
  int are_bibtex_full_templates_to_be_printed = 0;

  int are_names_to_be_printed = 0;
  int are_keywords_to_be_printed = 0;

  int use_icons = -1;
  int use_html_links = -1;

  /* what is needed for processing
   */
  typeListOfStrings list_of_strings;
  typeListOfItems list_of_items;
  typeListOfNames list_of_names;
  typeListOfNames list_of_indexed_names;
  typeListOfNames list_of_equivalent_names;
  typeListOfNames list_of_keywords;
  typeListOfNames list_of_indexed_keywords;
  typeListOfNames list_of_equivalent_keywords;

  int nb_authors, nb_diff_authors;
  int nb_keywords, nb_diff_keywords;

  /* what is needed for sorting
   */
  enumSortCriterium sortCriterium = _NONE_;

  /* files, directories management
   */
  int can_write;
  int check_icons = 0;
  int copy_icons = 0;
  int create_directories = 0;
  char *iconsdir = (char *)NULL;

  /* where to write
   */
  FILE *STDOUTPUT = stderr;
  char end_of_line = '\r';
  char *single_output = NULL;
  FILE *fd = NULL;
  FILE *index = NULL;
  char filename[256];
  char *file_where_to_print = NULL;
  FILE *fp = NULL;

  /* reading relevant variables
   */
  enumPredefinedStyle *input_file = NULL;
  enumPredefinedStyle  input_style = _BIBTEX_;
  int n_files;
  int i, t, n;
  

  /* other variables
   */
  /*

  FILE *g;
  
  char *anchor_name = "NAME";
  char *anchor_keyw = "KEYW";
  

  int put_first_letter_in_authors_list = 1;
  int put_first_letter_in_keywords_list = 1;
  int generate_complete_bibliography = 1;

  char str[256];
  time_t ti = time( NULL );
  struct passwd *pwd = NULL;
  */



  /**************************************************
   *
   * no arg, print help
   * 
   **************************************************/

  if ( argc == 1 ) {
    usage( argv[0], NULL, 0 );
    return( 0 );
  }
  for ( i=1; i<argc; i++ ) {

    if ( argv[i][0] == '-' ) {

      /****************************************
       *
       * general options
       *
       ****************************************/

      if ( strcmp ( argv[i], "-help" ) == 0  || 
	   strcmp ( argv[i], "--help" ) == 0 ) {
	usage( argv[0], NULL, 1 );
	return( 0 );
      }
      else if ( strcmp ( argv[i], "-version" ) == 0 ) {
	fprintf( stdout, "%s\n", version );
	return( 0 );
      }

      else if ( strcmp ( argv[i], "-verbose" ) == 0 || 
	   (strcmp ( argv[i], "-v" ) == 0 && argv[i][2] == '\0') ) {
	_verbose_ = 1;
	set_verbose_in_custom();
	set_verbose_in_read();
	set_verbose_in_write();
      }
      else if ( strcmp ( argv[i], "-no-verbose" ) == 0 || 
	   (strcmp ( argv[i], "-nv" ) == 0 && argv[i][3] == '\0') ) {
	_verbose_ = 0;
	unset_verbose_in_custom();
	unset_verbose_in_read();
 	unset_verbose_in_write();
      }
      else if ( strcmp ( argv[i], "-warning" ) == 0 || 
	   (strcmp ( argv[i], "-w" ) == 0 && argv[i][2] == '\0') ) {
	set_warning_in_read();
	set_verbose_in_custom();
	set_verbose_in_read();
	set_verbose_in_write();
      }
      else if ( strcmp ( argv[i], "-no-warning" ) == 0 || 
	   (strcmp ( argv[i], "-nw" ) == 0 && argv[i][3] == '\0') ) {
	unset_warning_in_read();
	if ( 0 ) {
	  unset_verbose_in_custom();
	  unset_verbose_in_read();
	  unset_verbose_in_write();
	}
      }
      else if ( strcmp ( argv[i], "-debug" ) == 0 || 
		(strcmp ( argv[i], "-D" ) == 0 && argv[i][2] == '\0') ) {
	set_debug_in_read();
	set_debug_in_write();
      }
       
      else if ( strcmp ( argv[i], "-stderr" ) == 0 ) {
	STDOUTPUT = stderr;
	end_of_line = '\r';
	set_verbose_on_stderr_in_write();
      }
      else if ( strcmp ( argv[i], "-stdout" ) == 0 ) {
	STDOUTPUT = stdout;
	end_of_line = '\n';
	set_verbose_on_stdout_in_write();
      }
    }

  }




  /**************************************************
   *
   * read args #1
   * 
   **************************************************/




  /**************************************************
   *
   * initialisation of customization
   * 
   **************************************************/
  init_environment_description  ( &desc_environment );
  init_generic_file_description ( &desc_generic_file );
  init_specific_file_description( &desc_single_file );
  init_specific_file_description( &desc_index_file );
  init_specific_file_description( &desc_author_file );
  init_specific_file_description( &desc_category_file );
  init_specific_file_description( &desc_keyword_file );
  init_specific_file_description( &desc_reduced_year_file );
  init_specific_file_description( &desc_complete_year_file );
  init_specific_file_description( &desc_complete_biblio_file );
  init_specific_file_description( &desc_default_file );
  init_bibtex_item_description  ( &desc_bibitem );

  customization_strings = build_customization_strings( &desc_environment,
						       &desc_generic_file,
						       &desc_single_file,
						       &desc_index_file,
						       &desc_author_file,
						       &desc_category_file,
						       &desc_keyword_file,
						       &desc_reduced_year_file,
						       &desc_complete_year_file,
						       &desc_complete_biblio_file,
						       &desc_default_file,
						       &desc_bibitem );

  if ( customization_strings == NULL ) {
    usage( argv[0], "unable to build customization strings", 0 );
    return( 0 );
  }
  
  load_default_style( customization_strings,
		      &desc_single_file,
		      &desc_index_file,
		      &desc_author_file,
		      &desc_category_file,
		      &desc_keyword_file,
		      &desc_reduced_year_file,
		      &desc_complete_year_file,
		      &desc_complete_biblio_file,
		      &desc_default_file );



  /* get the 'from' icons directory 
     from compilation
  */
#if defined( ICONSDIR )
  i = 0;
  while (  strncmp( customization_strings[i].key, "iconsdir", 8 ) != 0 && 
	   strncmp( customization_strings[i].key, "XXX", 3 ) != 0 ) {
    i ++;
  }
  if ( update_string( (char**)customization_strings[i].arg, ICONSDIR, 1 ) != 1 ) {
    fprintf (stderr, "unable to set 'iconsdir' from '%s'\n", ICONSDIR );
    return( 1 );
  }
#endif

  /* get the 'from' icons directory 
     from the environment
  */
  {
    int f=-1;
    i = 0;
    while ( arge[i] != NULL && f < 0 ) {
      if ( strncmp( arge[i], "BIBTEX_ICONSDIR", 15 ) == 0 )
	f = i;
      i++;
    }
    if ( f >= 0 && strlen( arge[f] ) > 16 ) {
      /* fprintf (stderr, "'%s'\n", arge[f] ); */
      i=0;
      while (  strncmp( customization_strings[i].key, "iconsdir", 8 ) != 0 && 
	       strncmp( customization_strings[i].key, "XXX", 3 ) != 0 ) {
	i ++;
      }
      if ( update_string( (char**)customization_strings[i].arg, arge[f]+16, 1 ) != 1 ) {
	fprintf (stderr, "unable to set 'iconsdir' from '%s'\n", arge[f] );
	return( 1 );
      }
    }
  }





  /**************************************************
   *
   * read args
   * 
   **************************************************/

  input_file = (enumPredefinedStyle*)MALLOC( argc * sizeof(enumPredefinedStyle), "main" );
  if ( input_file == NULL ) {
    fprintf( stderr, "unable to allocate some ints ?!\n" );
    return( 1 );
  }
  
  for (i=0; i<argc; i++ ) input_file[i] = _NO_STYLE_;

  /* parsing args
   */
  for ( i=1; i<argc; i++ ) {

    if ( argv[i][0] == '-' ) {

      /****************************************
       *
       * some special writing
       *
       ****************************************/

      if ( strcmp ( argv[i], "-print-customization-keys" ) == 0 
		|| strcmp ( argv[i], "-print-customisation-keys" ) == 0 
		|| strcmp ( argv[i], "-print-keys" ) == 0 ) {
	are_customization_keys_to_be_printed = 1;
      }
      
      else if ( strcmp ( argv[i], "-print-context" ) == 0 ) {
	is_context_to_be_printed = 1;
      }
      else if ( strcmp ( argv[i], "-print-full-context" ) == 0 ) {
	is_full_context_to_be_printed = 1;
      }
      else if ( strcmp ( argv[i], "-print-default-context" ) == 0 ) {
	is_default_context_to_be_printed = 1;
      }

      else if ( strcmp ( argv[i], "-print-bibtex-fields" ) == 0 ) {
	are_bibtex_fields_to_be_printed = 1;
      }
      else if ( strcmp ( argv[i], "-print-bibtex-all-fields" ) == 0 ) {
	are_bibtex_all_fields_to_be_printed = 1;
      }
      
      else if ( strcmp ( argv[i], "-print-bibtex-templates" ) == 0 ) {
	are_bibtex_templates_to_be_printed = 1;
      }
      else if ( strcmp ( argv[i], "-print-bibtex-full-templates" ) == 0 ) {
	are_bibtex_full_templates_to_be_printed = 1;
      }
      
      else if ( strcmp ( argv[i], "-print-names" ) == 0 ) {
	are_names_to_be_printed = 1;
      }
      else if ( strcmp ( argv[i], "-print-keywords" ) == 0 ) {
	are_keywords_to_be_printed = 1;
      }
      else if ( strcmp ( argv[i], "-print-file" ) == 0 ) {
	i += 1;
	if ( i >= argc) {
	  usage( argv[0], "missing arg to -print-file", 0 );
	  return( 1 );
	}
	file_where_to_print = argv[i];
      }

      /****************************************
       *
       * customization relevant options
       *
       ****************************************/
      else if ( strcmp ( argv[i], "-style" ) == 0 ) {
	i += 1;
	if ( i >= argc) {
	  usage( argv[0], "missing arg to -style", 0 );
	  return( 1 );
	}
	if ( strcmp ( argv[i], "no-style" ) == 0 ) {
	  load_style = _NO_STYLE_;
	}
	else if ( strcmp ( argv[i], "bibtex" ) == 0 ) {
	  load_style = _BIBTEX_;
	}
	else if ( strcmp ( argv[i], "html" ) == 0 ) {
	  load_style = _HTML_;
	}
	else if ( strcmp ( argv[i], "latex" ) == 0 ) {
	  load_style = _LATEX_;
	}
	else if ( strcmp ( argv[i], "layout" ) == 0 ) {
	  load_style = _LAYOUT_;
	}
	else if ( strcmp ( argv[i], "txt" ) == 0 ) {
	  load_style = _TXT_;
	}
	else {
	  fprintf( stderr, "unknown style: '%s'\n", argv[i] );
	}
      }

      else if ( strcmp ( argv[i], "-icons" ) == 0 
		|| strcmp ( argv[i], "-use-icons" ) == 0 ) {
	use_icons = 1;
      }
      else if ( strcmp ( argv[i], "-no-icons" ) == 0 ) {
	use_icons = 0;
      }
      else if ( strcmp ( argv[i], "-html-links" ) == 0 
		|| strcmp ( argv[i], "-use-html-links" ) == 0 ) {
	use_html_links = 1;
      }
      else if ( strcmp ( argv[i], "-no-html-links" ) == 0 ) {
	use_html_links = 0;
      }
      else if ( strcmp ( argv[i], "-iconsdir" ) == 0 ) {
	i += 1;
	if ( i >= argc) {
	  usage( argv[0], "missing arg to -iconsdir", 0 );
	  return( 1 );
	}
	iconsdir = argv[i];
	copy_icons = 1;
	use_icons = 1;
      }
      else if ( strcmp ( argv[i], "-copy-icons" ) == 0 ) {
	copy_icons = 1;
      }
      else if ( strcmp ( argv[i], "-check-icons" ) == 0 ) {
	check_icons = 1;
	use_icons = 1;
      }
     
      /****************************************
       *
       * where to write (only here at the moment)
       *
       ****************************************/
      else if ( strcmp ( argv[i], "-create-directories" ) == 0 
		|| strcmp ( argv[i], "-force" ) == 0 ) {
	create_directories = 1;
      }

      else if ( strcmp ( argv[i], "-single-output" ) == 0 
		|| strcmp ( argv[i], "-output" ) == 0 ) {
	i += 1;
	if ( i >= argc) {
	  usage( argv[0], "missing arg to -single-output", 0 );
	  return( 1 );
	}
	single_output = argv[i];
      }
      

      /****************************************
       *
       * sort options
       *
       ****************************************/
      else if ( strcmp ( argv[i], "-sort-criterium" ) == 0 
		|| (strcmp ( argv[i], "-sort" ) == 0 && argv[i][5] == '\0') ) {
	i += 1;
	if ( i >= argc) {
	  usage( argv[0], "missing arg to -sort-criterium", 0 );
	  return( 1 );
	}
	if ( strcmp ( argv[i], "cat" ) == 0 ) {
	  sortCriterium = _CATEGORY_;
	}
	else if ( strcmp ( argv[i], "year" ) == 0 ) {
	  sortCriterium = _YEAR_;
	}
	else if ( strcmp ( argv[i], "name" ) == 0 ) {
	  sortCriterium = _NAME_;
	}
	else if ( strcmp ( argv[i], "type" ) == 0 ) {
	  sortCriterium = _TYPE_;
	}
	else if ( strcmp ( argv[i], "bibkey" ) == 0 ) {
	  sortCriterium = _BIBKEY_;
	}
	else {
	  fprintf( stderr, "unknown sort criterium: '%s'\n", argv[i] );
	}
      }
      else if ( strcmp ( argv[i], "-partial-equality-of-names" ) == 0 ||
		  strcmp ( argv[i], "-peon" ) == 0 ) {
	set_partial_equality_of_names();
	
      }
      else if ( strcmp ( argv[i], "-complete-equality-of-names" ) == 0 ||
		  strcmp ( argv[i], "-ceon" ) == 0 ) {
	set_complete_equality_of_names();
      }

      
      else if ( strcmp ( argv[i], "-bibtex" ) == 0 ) {
	input_style = _BIBTEX_;
      }
      else if ( strcmp ( argv[i], "-medline" ) == 0 ) {
	input_style = _MEDLINE_;
      }


      /****************************************
       *
       * other options
       *
       ****************************************/

      else {
	if ( !( strcmp ( argv[i], "-help" ) == 0  
		|| strcmp ( argv[i], "--help" ) == 0 
		|| strcmp ( argv[i], "-verbose" ) == 0 
		|| (strcmp ( argv[i], "-v" ) == 0 && argv[i][2] == '\0')
		|| strcmp ( argv[i], "-no-verbose" ) == 0 
		|| (strcmp ( argv[i], "-nv" ) == 0 && argv[i][3] == '\0')
		|| strcmp ( argv[i], "-warning" ) == 0 
		||  (strcmp ( argv[i], "-w" ) == 0 && argv[i][2] == '\0')
		|| strcmp ( argv[i], "-no-warning" ) == 0 
		|| (strcmp ( argv[i], "-nw" ) == 0 && argv[i][3] == '\0')
		|| strcmp ( argv[i], "-stderr" ) == 0 
		|| strcmp ( argv[i], "-stdout" ) == 0
		|| strcmp ( argv[i], "-debug" ) == 0 
		|| (strcmp ( argv[i], "-D" ) == 0 && argv[i][2] == '\0') ) ) {
	  fprintf( stderr, "unknown option: '%s'\n", argv[i] );
	}
      }
    }
    else {
      /* should be a file
       */
      input_file[i] = input_style;
    }
  }



  
  /**************************************************
   *
   * stuff to be printed out
   *
   **************************************************/
  
  if ( are_bibtex_fields_to_be_printed ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    print_bibtex_fields( fp );

    if ( file_where_to_print != NULL ) fclose( fp );
    return( 0 );

  }



  if ( are_bibtex_all_fields_to_be_printed ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    print_bibtex_all_fields( fp );

    if ( file_where_to_print != NULL ) fclose( fp );
    return( 0 );

  }



  if ( are_bibtex_templates_to_be_printed ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    print_bibtex_templates( fp );

    if ( file_where_to_print != NULL ) fclose( fp );
    return( 0 );

  }



  if ( are_bibtex_full_templates_to_be_printed ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    print_bibtex_full_templates( fp );

    if ( file_where_to_print != NULL ) fclose( fp );
    return( 0 );

  }



  if ( are_customization_keys_to_be_printed ) {
    
    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    print_customization_strings_keys( fp, customization_strings );

    if ( file_where_to_print != NULL ) fclose( fp );
    return( 0 );

  }





  /**************************************************
   *
   * everything is now known
   * read what should be read
   * update what should be updated
   *
   **************************************************/
  for ( n_files=0, i=1; i<argc; i++ ) {
    switch ( input_file[i] ) {
    default : break;
    case _BIBTEX_ :
    case _MEDLINE_ :
      n_files ++;
    }
  }


  /* load default style
   */
  load_predefined_style( customization_strings, load_style );
  if( is_default_context_to_be_printed 
      || (n_files == 0 && is_context_to_be_printed) 
      || (n_files == 0 && is_full_context_to_be_printed) ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    print_customization_strings( fp, customization_strings,
				 is_full_context_to_be_printed );
    
    if ( file_where_to_print != NULL ) fclose( fp );
    return( 0 );
  }


  
  if ( n_files == 0 ) {
    usage( argv[0], "no files to read?", 0 );
    return( 1 );
  }



  /* read files
   *
   * list_of_indexed_names will contains the list of names
   * to be indexed (given by strings in files)
   */
  init_list_of_strings( &list_of_strings );
  init_list_of_items( &list_of_items );
  init_list_of_names( &list_of_indexed_names );
  init_list_of_names( &list_of_equivalent_names );
  init_list_of_names( &list_of_indexed_keywords );
  init_list_of_names( &list_of_equivalent_keywords );



  for ( t=0, i=1; i<argc; i++ ) {

    switch ( input_file[i] ) {
    default : break;
    case _BIBTEX_ :
      n = ReadBibtexInputs( &list_of_items,  &list_of_strings,
			    &list_of_indexed_names, 
			    &list_of_equivalent_names,
			    &list_of_indexed_keywords, 
			    &list_of_equivalent_keywords,
			    customization_strings, argv[i] );
      if ( n >= 0 ) {
	if ( _verbose_ )
	  fprintf( STDOUTPUT, " read %5d items in '%s'\n", n, argv[i] );
	t += n;
      }
      else {
	fprintf( stderr, "Error: when reading '%s' ...\n", argv[i] );
	exit( 1 );
      }
      break;
    case _MEDLINE_ :
      n = ReadMedlineInputs( &list_of_items,  &list_of_strings,
			     &list_of_indexed_names, &list_of_equivalent_names,
			     &list_of_indexed_keywords, 
			     &list_of_equivalent_keywords,
			     customization_strings, argv[i] );
      if ( n >= 0 ) {
	if ( _verbose_ )
	  fprintf( STDOUTPUT, " read %5d items in '%s'\n", n, argv[i] );
	t += n;
      }
      else {
	fprintf( stderr, "Error: when reading '%s' ...\n", argv[i] );
	exit( 1 );
      }
      break;
    }

    /* if the output IS a single file
       keep all the found strings
       else reinitialize them, because of the possible double
    */
    if ( single_output == NULL ) {
      release_list_of_strings( &list_of_strings );
      init_list_of_strings( &list_of_strings );
    }
  }

  if ( _verbose_ )
    fprintf( STDOUTPUT, "found %5d items\n", t );

  FREE( input_file, "main" );

  if ( t == 0  ) {
    usage( argv[0], "no valid items, was there bibtex files?", 0 );
    exit( 1 );
  }



  /* last chance to change the 'from' icons directory 
     online option overrides all
   */
  if ( iconsdir != NULL ) {
    if ( update_string( (char**)customization_strings[i].arg, iconsdir, 1 ) != 1 ) {
      fprintf (stderr, "unable to set 'iconsdir' from '%s'\n", iconsdir );
      exit( 1 );
    }
  }



  

  /**************************************************
   *
   * BUILD LISTS
   *
   * build other lists
   * it is not necessary if we only need one single file
   * but it will be done ...
   *
   **************************************************/

  /* found names
   */
  init_list_of_names( &list_of_names );
  nb_authors = retrieve_all_names( &list_of_names, &list_of_items );
  sort_names( list_of_names.name, 0, list_of_names.n-1 );
  sort_names( list_of_indexed_names.name, 0, list_of_indexed_names.n-1 );
  sort_names( list_of_equivalent_names.name, 0, list_of_equivalent_names.n-1 );
  nb_diff_authors = retrieve_names_to_be_indexed( &list_of_names, 
						  &list_of_indexed_names, 
						  &list_of_equivalent_names );
  if ( _verbose_ )
    fprintf( STDOUTPUT, "found %5d different names among %5d authors\n", 
	     nb_diff_authors, nb_authors );
  update_names( &list_of_items, &list_of_names, &list_of_indexed_names );



  if ( are_names_to_be_printed ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    fprintf( fp, "\n\n === Authors ===\n" );
    fprintf( fp, " ===============\n" );
    print_list_of_names( fp, &list_of_names );
    fprintf( fp, "\n\n === Indexed authors ===\n" );
    fprintf( fp, " =======================\n" );
    print_list_of_names( fp, &list_of_indexed_names );

    if ( file_where_to_print != NULL ) fclose( fp );

  }



  /* found keywords
   */
  init_list_of_names( &list_of_keywords );
  nb_keywords = retrieve_all_keywords( &list_of_keywords, &list_of_items );
  sort_names( list_of_keywords.name, 0, list_of_keywords.n-1 );
  sort_names( list_of_indexed_keywords.name, 0, list_of_indexed_keywords.n-1 );
  sort_names( list_of_equivalent_keywords.name, 0, list_of_equivalent_keywords.n-1 );
  nb_diff_keywords = retrieve_names_to_be_indexed( &list_of_keywords, 
						   &list_of_indexed_keywords, 
						   &list_of_equivalent_keywords );
  if ( _verbose_ )
    fprintf( STDOUTPUT, "found %5d different keywords among %5d keywords\n", 
	     nb_diff_keywords, nb_keywords );
  update_keywords( &list_of_items, &list_of_keywords, NULL );



  if ( are_keywords_to_be_printed ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    fprintf( fp, "\n\n === Keywords ===\n" );
    fprintf( fp, " ================\n" );
    print_list_of_names( fp, &list_of_keywords );
    fprintf( fp, "\n\n === Indexed keywords ===\n" );
    fprintf( fp, " ========================\n" );
    print_list_of_names( fp, &list_of_indexed_keywords );

    if ( file_where_to_print != NULL ) fclose( fp );

  }


  if ( _verbose_ )
    fprintf( STDOUTPUT, "\n" );



  /* update environment with on-line options
     update file descriptions with default that may have be modified
     while reading files
  */

  if ( use_html_links == 1 ) {
    desc_environment.use_html_links_between_pages = 1;

    desc_environment.use_html_links_towards_authors_pages = 1;
    desc_environment.use_html_links_towards_categories_pages = 1;
    desc_environment.use_html_links_towards_reduced_years_pages = 1;
    desc_environment.use_html_links_towards_complete_years_pages = 1;
    desc_environment.use_html_links_towards_keywords_pages = 1;
  
    desc_environment.use_html_links_towards_urls = 1;
  }
  if ( use_html_links == 0 ) {
    desc_environment.use_html_links_between_pages = 0;

    desc_environment.use_html_links_towards_authors_pages = 0;
    desc_environment.use_html_links_towards_categories_pages = 0;
    desc_environment.use_html_links_towards_reduced_years_pages = 0;
    desc_environment.use_html_links_towards_complete_years_pages = 0;
    desc_environment.use_html_links_towards_keywords_pages = 0;
  
    desc_environment.use_html_links_towards_urls = 0;
  }

  if ( use_icons == 1 ) {
    desc_environment.use_icons = 1;
    desc_bibitem.bib_doi.use_icon = 1;
    desc_bibitem.bib_pdf.use_icon = 1;
    desc_bibitem.bib_pmid.use_icon = 1;
    desc_bibitem.bib_postscript.use_icon = 1;
    desc_bibitem.bib_url.use_icon = 1;
    desc_bibitem.bib_url_hal.use_icon = 1;
  }
  if ( use_icons == 0 ) {
    desc_environment.use_icons = 0;
    desc_bibitem.bib_doi.use_icon = 0;
    desc_bibitem.bib_pdf.use_icon = 0;
    desc_bibitem.bib_pmid.use_icon = 0;
    desc_bibitem.bib_postscript.use_icon = 0;
    desc_bibitem.bib_url.use_icon = 0;
    desc_bibitem.bib_url_hal.use_icon = 0;
  }

  update_file_description_with_default( &desc_single_file,           
					&desc_default_file );
  update_file_description_with_default( &desc_index_file,           
					&desc_default_file );
  update_file_description_with_default( &desc_author_file,          
					&desc_default_file );
  update_file_description_with_default( &desc_category_file,        
					&desc_default_file );
  update_file_description_with_default( &desc_keyword_file,         
					&desc_default_file );
  update_file_description_with_default( &desc_reduced_year_file,    
					&desc_default_file );
  update_file_description_with_default( &desc_complete_year_file,   
					&desc_default_file );
  update_file_description_with_default( &desc_complete_biblio_file, 
					&desc_default_file );

  if ( is_context_to_be_printed || is_full_context_to_be_printed ) {

    if ( file_where_to_print != NULL ) {
      fp = fopen( file_where_to_print, "w" );
      if ( fp == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing\n", file_where_to_print );
	return( 1 );
      }
    }
    else 
      fp = stdout;
    
    print_customization_strings( fp, customization_strings, 
				 is_full_context_to_be_printed );

    if ( file_where_to_print != NULL ) fclose( fp );
    return( 0 );
  }









  /**************************************************
   *
   * only a sorted output is required
   * (useful while merging files with redondancy)
   *
   **************************************************/
  
  if ( single_output != NULL ) {

    desc_environment.use_html_links_between_pages = 0;

    desc_environment.use_html_links_towards_authors_pages = 0;
    desc_environment.use_html_links_towards_categories_pages = 0;
    desc_environment.use_html_links_towards_reduced_years_pages = 0;
    desc_environment.use_html_links_towards_complete_years_pages = 0;
    desc_environment.use_html_links_towards_keywords_pages = 0;
  
    desc_environment.use_icons = 0;

    switch( load_style ) {
    case _HTML_ : break;
    default :
      desc_environment.use_html_links_towards_urls = 0;
    }

    fprintf( STDOUTPUT, "generating %s\n", single_output );

    if ( single_output[0] == '-' && single_output[0] != '\0' )
      fd = stdout;
    else 
      fd = fopen( single_output, "w" );

    if ( fd == NULL ) {
      fprintf (stderr, "unable to open '%s' for writing single output\n", single_output );
      return( 1 );
    }

    sort_list_of_items( &list_of_items, sortCriterium );

    switch ( load_style ) {
    default :
      write_all_items( fd, &list_of_items, sortCriterium,
		       &desc_environment, &desc_generic_file, 
		       &desc_single_file,
		       &desc_bibitem, 0 );
      break;
    case _BIBTEX_ :
      print_bibliography( fd, &list_of_items, &list_of_strings,
			  argc, argv );
      break;
    }

    if ( single_output[0] != '-' ) fclose( fd );

    release_list_of_strings( &list_of_strings );
    release_list_of_names( &list_of_equivalent_names );
    release_list_of_names( &list_of_indexed_names );
    release_list_of_names( &list_of_names );
    release_list_of_names( &list_of_equivalent_keywords );
    release_list_of_names( &list_of_indexed_keywords );
    release_list_of_names( &list_of_keywords );
    release_list_of_items( &list_of_items );

    return( 0 );
  }








  /**************************************************
   *
   * WRITING OUTPUT
   *
   **************************************************/
  
  /*
   * check, copy icons
   */
  if ( use_icons == 1 || desc_environment.use_icons ) {

    if ( copy_icons == 1 ) {

      /* check the 'from' directory
       */
      
      can_write = 1;
    
      if ( create_directories == 1 ) {
	if ( make_directory( desc_environment.directory_icons ) != 1 ) {
	  fprintf( stderr, "unable to create icons '%s',",
		   desc_environment.directory_icons );
	  fprintf( stderr, "... will not copy icons\n");
	  can_write = 0;
	}
      }
      else {
	if ( check_directory( desc_environment.directory_icons ) != 1 ) {
	  fprintf( stderr, "    pb with icons '%s',",
		   desc_environment.directory_icons );
	  fprintf( stderr, "... will not copy icons\n");
	  can_write = 0;
	}
      }
      
      if ( can_write == 1 ) {

	copy_all_icons( &desc_environment, &desc_bibitem );
      }

    }
    else if ( check_icons == 1 ) {
      check_all_icons( &desc_environment, &desc_bibitem, 
		       desc_environment.directory_icons );
    }
  }


  /* check whether all links are relevant
   */
  if ( ! desc_reduced_year_file.create_pages )
    desc_environment.use_html_links_towards_reduced_years_pages = 0;
  if ( ! desc_complete_year_file.create_pages )
    desc_environment.use_html_links_towards_complete_years_pages = 0;
  if ( desc_category_file.create_pages ) 
    desc_environment.use_html_links_towards_categories_pages = 0;
  if ( ! desc_author_file.create_pages )
    desc_environment.use_html_links_towards_authors_pages = 0;
  if ( ! desc_keyword_file.create_pages ) 
    desc_environment.use_html_links_towards_keywords_pages = 0;

  /* open index for writing
   */
  if ( desc_index_file.create_pages ) {

    sprintf( filename, "%s.%s", desc_environment.filename_index, 
	     desc_environment.filename_extension );
    index = fopen( filename, "w" );
    if ( index == NULL ) {
      fprintf( stderr, "unable to open '%s.%s' for writing index\n", 
	       desc_environment.filename_index, 
	       desc_environment.filename_extension );
      usage( argv[0], NULL, 0 );
      return( 0 );
    }

    write_begin_of_file( index, &desc_generic_file, 
			 &desc_index_file, desc_index_file.file_title, 
			 desc_index_file.page_title );

  }

  /* selection by year
   */
  if ( desc_reduced_year_file.create_pages 
       || desc_complete_year_file.create_pages ) {

    can_write = 1;
    
    if ( create_directories == 1 ) {
      if ( make_directory( desc_environment.directory_years ) != 1 ) {
	fprintf( stderr, "unable to create directory '%s',",
		 desc_environment.directory_years );
	fprintf( stderr, "... will not write years\n");
	can_write = 0;
      }
    }
    else {
      if ( check_directory( desc_environment.directory_years ) != 1 ) {
	fprintf( stderr, "    pb with directory '%s',",
		 desc_environment.directory_years );
	fprintf( stderr, "... will not write years\n");
	can_write = 0;
      }
    }
    
    if ( can_write == 1 ) {

      if ( desc_reduced_year_file.build_index 
	   || desc_complete_year_file.build_index )
	write_item_customized_string( index, "Selection by year",
				      &(desc_generic_file.page_subtitle_tag), 
				      NULL );
      
      if ( desc_reduced_year_file.create_pages )
	write_all_years( index, &list_of_items, 
			 &desc_environment, &desc_generic_file, 
			 &desc_reduced_year_file,
			 &desc_bibitem, (load_style == _BIBTEX_) );
      
      if ( desc_complete_year_file.create_pages )
	write_all_years( NULL, &list_of_items, 
			 &desc_environment, &desc_generic_file, 
			 &desc_complete_year_file,
			 &desc_bibitem, (load_style == _BIBTEX_) );
      
    }

  }


  /* selection by category
   */

  if ( desc_category_file.create_pages ) {

    can_write = 1;
    
    if ( create_directories == 1 ) {
      if ( make_directory( desc_environment.directory_categories ) != 1 ) {
	fprintf( stderr, "unable to create directory '%s',",
		 desc_environment.directory_categories );
	fprintf( stderr, "... will not write categories\n");
	can_write = 0;
      }
    }
    else {
      if ( check_directory( desc_environment.directory_categories ) != 1 ) {
	fprintf( stderr, "    pb with directory '%s',",
		 desc_environment.directory_categories );
	fprintf( stderr, "... will not write categories\n");
	can_write = 0;
      }
    }
    
    if ( can_write == 1 ) {

      if ( desc_category_file.build_index )
	write_item_customized_string( index, "Selection by category",
				      &(desc_generic_file.page_subtitle_tag), 
				      NULL );
      
      write_all_categories( index, &list_of_items,
			    &desc_environment, &desc_generic_file, 
			    &desc_category_file,
			    &desc_bibitem, (load_style == _BIBTEX_) );
      
    }

  }


  /* selection by author
   */
  if ( desc_author_file.create_pages ) {
    
    can_write = 1;
    
    if ( create_directories == 1 ) {
      if ( make_directory( desc_environment.directory_authors ) != 1 ) {
	fprintf( stderr, "unable to create directory '%s',",
		 desc_environment.directory_authors );
	fprintf( stderr, "... will not write authors\n");
	can_write = 0;
      }
    }
    else {
      if ( check_directory( desc_environment.directory_authors ) != 1 ) {
	fprintf( stderr, "    pb with directory '%s',",
		 desc_environment.directory_authors );
	fprintf( stderr, "... will not write authors\n");
	can_write = 0;
      }
    }
    
    if ( can_write == 1 ) {

      if ( list_of_indexed_names.n != 1 
	   || (list_of_indexed_names.n == 1 && list_of_indexed_names.name[0].status != TO_BE_INDEXED ) ) {
      
	if ( desc_category_file.build_index )
	  write_item_customized_string( index, "Selection by author",
					&(desc_generic_file.page_subtitle_tag), 
					NULL );
      
      }
      
      write_all_authors( index, &list_of_items, &list_of_names,
			 &desc_environment, &desc_generic_file, 
			 &desc_author_file,
			 &desc_bibitem, (load_style == _BIBTEX_) );
      
    }
    
  }
  


  /* selection by keyword
   */
  if ( desc_keyword_file.create_pages && list_of_keywords.n > 0 ) {

    can_write = 1;
    
    if ( create_directories == 1 ) {
      if ( make_directory( desc_environment.directory_keywords ) != 1 ) {
	fprintf( stderr, "unable to create directory '%s',",
		 desc_environment.directory_keywords );
	fprintf( stderr, "... will not write keywords\n");
	can_write = 0;
      }
    }
    else {
      if ( check_directory( desc_environment.directory_keywords ) != 1 ) {
	fprintf( stderr, "    pb with directory '%s',",
		 desc_environment.directory_keywords );
	fprintf( stderr, "... will not write keywords\n");
	can_write = 0;
      }
    }
    
    if ( can_write == 1 ) {

      if ( list_of_indexed_keywords.n != 1 ) {
	
	if ( desc_category_file.build_index )
	  write_item_customized_string( index, "Selection by keyword",
					&(desc_generic_file.page_subtitle_tag), 
					NULL );
	
      }
      
      write_all_keywords( index, &list_of_items, &list_of_keywords,
			  &desc_environment, &desc_generic_file, 
			  &desc_keyword_file,
			  &desc_bibitem, (load_style == _BIBTEX_) );
      
    }

  }
    



  /* complete bibliography generation
   */
  if ( desc_complete_biblio_file.create_pages ) {
    
    can_write = 1;
    
    if ( create_directories == 1 ) {
      if ( make_directory( desc_environment.directory_biblio ) != 1 ) {
	fprintf( stderr, "unable to create directory '%s',",
		 desc_environment.directory_biblio );
	fprintf( stderr, "... will not write complete biblio\n");
	can_write = 0;
      }
    }
    else {
      if ( check_directory( desc_environment.directory_biblio ) != 1 ) {
	fprintf( stderr, "    pb with directory '%s',",
		 desc_environment.directory_biblio );
	fprintf( stderr, "... will not write complete biblio\n");
	can_write = 0;
      }
    }
    
    if ( can_write == 1 ) {

      sprintf( filename, "%s/%s.%s", desc_environment.directory_biblio,
	       desc_environment.filename_complete_biblio,
	       desc_environment.filename_extension );
      fd = fopen( filename, "w" );
      if ( fd == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing complete bibliograhy\n", filename );
	return( 1 );
      }
      write_all_items( fd, &list_of_items, sortCriterium, 
		       &desc_environment, &desc_generic_file, 
		       &desc_complete_biblio_file,
		       &desc_bibitem, (load_style == _BIBTEX_) );
      fclose( fd );
      
      if ( desc_environment.use_html_links_between_pages ) {

	write_item_customized_string( index, "Complete bibliography",
				      &(desc_generic_file.page_subtitle_tag), 
				      NULL );
	write_string_or_file( index, desc_complete_biblio_file.index.table.start );
	write_string_or_file( index, desc_complete_biblio_file.index.row.start );
	write_string_or_file( index, desc_complete_biblio_file.index.cell.start );
	fprintf( index, "<a href=\"%s\">", filename );
	fprintf( index, "Complete bibliography as a single HTML page" );
	fprintf( index, "</a>\n" );
	write_string_or_file( index, desc_complete_biblio_file.index.cell.end );
	write_string_or_file( index, desc_complete_biblio_file.index.row.end );
	
      }

      sprintf( filename, "%s/%s.bib", desc_environment.directory_biblio,
	       desc_environment.filename_complete_biblio );
      fd = fopen( filename, "w" );
      if ( fd == NULL ) {
	fprintf (stderr, "unable to open '%s' for writing bibliography\n", filename );
	return( 1 );
      }
      sort_list_of_items( &list_of_items, sortCriterium );
      print_bibliography( fd, &list_of_items, &list_of_strings,
			  argc, argv );
      fclose( fd );
      
      if ( desc_environment.use_html_links_between_pages ) {
	
	write_string_or_file( index, desc_complete_biblio_file.index.row.start );
	write_string_or_file( index, desc_complete_biblio_file.index.cell.start );
	fprintf( index, "<a href=\"%s\">", filename );
	fprintf( index, "Complete bibliography as a single BIBTEX file" );
	fprintf( index, "</a>\n" );
	write_string_or_file( index, desc_complete_biblio_file.index.cell.end );
	write_string_or_file( index, desc_complete_biblio_file.index.row.end );
	
      }
    
      write_string_or_file( index, desc_complete_biblio_file.index.table.end );
    }
    
  }

  
  if ( desc_index_file.create_pages ) {

    write_end_of_file( index, &desc_generic_file, &desc_index_file );
    fclose( index );

  }  

  release_list_of_strings( &list_of_strings );
  release_list_of_names( &list_of_equivalent_names );
  release_list_of_names( &list_of_indexed_names );
  release_list_of_names( &list_of_names );
  release_list_of_names( &list_of_equivalent_keywords );
  release_list_of_names( &list_of_indexed_keywords );
  release_list_of_names( &list_of_keywords );
  release_list_of_items( &list_of_items );
  
  free_environment_description( &desc_environment );
  free_generic_file_description( &desc_generic_file );
  free_specific_file_description( &desc_single_file );
  free_specific_file_description( &desc_index_file );
  free_specific_file_description( &desc_author_file );
  free_specific_file_description( &desc_category_file );
  free_specific_file_description( &desc_keyword_file );
  free_specific_file_description( &desc_reduced_year_file );
  free_specific_file_description( &desc_complete_year_file );
  free_specific_file_description( &desc_complete_biblio_file );
  free_specific_file_description( &desc_default_file );
  free_bibtex_item_description( &desc_bibitem );

  FREE( customization_strings, "main" );

  return( 0 );
}
