/*************************************************************************
 * custom-layout.c - from bibtex2html distribution
 *
 * $Id: custom-layout.c,v 2.16 2004/07/20 15:19:35 greg Exp $
 *
 * Copyright � INRIA 2002, Gregoire Malandain
 *
 * The purpose of this software is to automatically produce html pages from 
 * bibtex files, and to provide access to the bibtex entries by several 
 * criteria: year of publication, category of publication and author name, 
 * from an index page. 
 * see http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html
 *
 * AUTHOR:
 * Gregoire Malandain (greg@sophia.inria.fr)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * CREATION DATE: 
 * 
 *
 * ADDITIONS, CHANGES
 *
 */


#include <custom-layout.h>

char *default_specific_file_description[] =
{ "@STRING{ default_create_pages = 1 }",
  "@STRING{ default_write_local_menu = 0 }",
  "@STRING{ default_build_initials_index = 0 }",
  "@STRING{ default_initials.cells_per_row = 14 }",
  "@STRING{ default_put_initials_in_index = 0 }",
  "@STRING{ default_write_anchor = 1 }",
  "@STRING{ default_write_disclaimer = 1 }",
  "@STRING{ default_write_date = 1 }",
  "@STRING{ default_write_author = 1 }",
  "@STRING{ default_write_credits = 1 }",
  
  "@STRING{ single_output_write_complete_suffix = 0 }",
  "@STRING{ single_output_file_title = \"All publications sorted by %s\" }",
  "@STRING{ single_output_default_file_title = \"All publications\" }",
  "@STRING{ single_output_page_title = \"All publications sorted by %s\" }",
  "@STRING{ single_output_default_page_title = \"All publications\" }",
  "@STRING{ single_output_write_bibtex_abstract = 0 }",
  "@STRING{ single_output_write_bibtex_annote   = 0 }",
  "@STRING{ single_output_write_bibtex_comments = 0 }",
  "@STRING{ single_output_write_bibtex_key      = 0 }",
  "@STRING{ single_output_write_bibtex_file     = 0 }",
  "@STRING{ single_output_write_bibtex_entry    = 0 }",
 
  "@STRING{ index_write_complete_suffix = 0 }",
  "@STRING{ index_file_title = \"List of publications: index\" }",
  "@STRING{ index_default_file_title = \"List of publications: index\" }",
  "@STRING{ index_page_title = \"List of publications: index\" }",
  "@STRING{ index_default_page_title = \"List of publications: index\" }",
  "@STRING{ index_write_bibtex_abstract = 0 }",
  "@STRING{ index_write_bibtex_annote   = 0 }",
  "@STRING{ index_write_bibtex_comments = 0 }",
  "@STRING{ index_write_bibtex_key      = 0 }",
  "@STRING{ index_write_bibtex_file     = 0 }",
  "@STRING{ index_write_bibtex_entry    = 0 }",
 
  "@STRING{ author_write_complete_suffix = 0 }",
  "@STRING{ author_file_title = \"Publications of %s %s\" }",
  "@STRING{ author_default_file_title = \"Publications of unknown author(s)\" }",
  "@STRING{ author_page_title = \"Publications of %s %s\" }",
  "@STRING{ author_default_page_title = \"Publications of unknown author(s)\" }",
  "@STRING{ author_index.cells_per_row   = 3 }",
  "@STRING{ author_write_bibtex_abstract = 1 }",
  "@STRING{ author_write_bibtex_annote   = 1 }",
  "@STRING{ author_write_bibtex_comments = 1 }",
  "@STRING{ author_write_bibtex_key      = 0 }",
  "@STRING{ author_write_bibtex_file     = 0 }",
  "@STRING{ author_write_bibtex_entry    = 1 }",
  
  "@STRING{ category_write_complete_suffix = 0 }",
  "@STRING{ category_file_title = \"%s\" }",
  "@STRING{ category_default_file_title = \"Unknown category\" }",
  "@STRING{ category_page_title = \"%s\" }",
  "@STRING{ category_default_page_title = \"Unknown category\" }",
  "@STRING{ category_index.cells_per_row   = 3 }",
  "@STRING{ category_write_bibtex_abstract = 1 }",
  "@STRING{ category_write_bibtex_annote   = 1 }",
  "@STRING{ category_write_bibtex_comments = 1 }",
  "@STRING{ category_write_bibtex_key      = 0 }",
  "@STRING{ category_write_bibtex_file     = 0 }",
  "@STRING{ category_write_bibtex_entry    = 1 }",
  
  "@STRING{ keyword_write_complete_suffix = 0 }",
  "@STRING{ keyword_file_title = \"Publications about '%s'\" }",
  "@STRING{ keyword_default_file_title = \"Unknown keyword\" }",
  "@STRING{ keyword_page_title = \"Publications about '%s'\" }",
  "@STRING{ keyword_default_page_title = \"Unknown keyword\" }",
  "@STRING{ keyword_index.cells_per_row   = 3 }",
  "@STRING{ keyword_write_bibtex_abstract = 1 }",
  "@STRING{ keyword_write_bibtex_annote   = 1 }",
  "@STRING{ keyword_write_bibtex_comments = 1 }",
  "@STRING{ keyword_write_bibtex_key      = 0 }",
  "@STRING{ keyword_write_bibtex_file     = 0 }",
  "@STRING{ keyword_write_bibtex_entry    = 1 }",
  
  "@STRING{ reduced_year_write_complete_suffix = 0 }",
  "@STRING{ reduced_year_file_title = \"Publications of year %d\" }",
  "@STRING{ reduced_year_default_file_title = \"Publications with no year\" }",
  "@STRING{ reduced_year_page_title = \"Publications of year %d\" }",
  "@STRING{ reduced_year_default_page_title = \"Publications with no year\" }",
  "@STRING{ reduced_year_index.cells_per_row   = 9 }",
  "@STRING{ reduced_year_write_bibtex_abstract = 1 }",
  "@STRING{ reduced_year_write_bibtex_annote   = 1 }",
  "@STRING{ reduced_year_write_bibtex_comments = 1 }",
  "@STRING{ reduced_year_write_bibtex_key      = 0 }",
  "@STRING{ reduced_year_write_bibtex_file     = 0 }",
  "@STRING{ reduced_year_write_bibtex_entry    = 1 }",
  
  "@STRING{ complete_year_write_complete_suffix = 1 }",
  "@STRING{ complete_year_file_title = \"Publications of year %d\" }",
  "@STRING{ complete_year_default_file_title = \"Publications with no year\" }",
  "@STRING{ complete_year_page_title = \"Publications of year %d\" }",
  "@STRING{ complete_year_default_page_title = \"Publications with no year\" }",
  "@STRING{ complete_year_index.cells_per_row   = 9 }",
  "@STRING{ complete_year_write_bibtex_abstract = 2 }",
  "@STRING{ complete_year_write_bibtex_annote   = 2 }",
  "@STRING{ complete_year_write_bibtex_comments = 2 }",
  "@STRING{ complete_year_write_bibtex_key      = 0 }",
  "@STRING{ complete_year_write_bibtex_file     = 0 }",
  "@STRING{ complete_year_write_bibtex_entry    = 2 }",
  
  "@STRING{ complete_biblio_write_complete_suffix = 0 }",
  "@STRING{ complete_biblio_file_title = \"All publications sorted by %s\" }",
  "@STRING{ complete_biblio_default_file_title = \"All publications\" }",
  "@STRING{ complete_biblio_page_title = \"All publications sorted by %s\" }",
  "@STRING{ complete_biblio_default_page_title = \"All publications\" }",
  "@STRING{ complete_biblio_index.cells_per_row   = 9 }",
  "@STRING{ complete_biblio_write_bibtex_abstract = 2 }",
  "@STRING{ complete_biblio_write_bibtex_annote   = 2 }",
  "@STRING{ complete_biblio_write_bibtex_comments = 2 }",
  "@STRING{ complete_biblio_write_bibtex_key      = 2 }",
  "@STRING{ complete_biblio_write_bibtex_file     = 0 }",
  "@STRING{ complete_biblio_write_bibtex_entry    = 1 }",
  
  "XXX"
};

  

  
  

/* some global constant
   for predefined styles
 */

char *default_generic_file_description_html[] = 
{ "@STRING{ file_tag.start = \"<html>\n\n\" }",
  "@STRING{ file_tag.end = \"\n</html>\n\" }",
  "@STRING{ head_tag.start = \"<head>\n\" }",
  "@STRING{ head_tag.end = \"\
<META http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n\
<META name=\"keywords\" lang=\"en\" content=\"bibtex2html, bibliography, article, report\">\n\
<META name=\"GENERATOR\" content=\"bibtex2html 1.01\">\n\
</head>\n\" }",
  "@STRING{ file_title_tag.start = \"<title>\n\" }",
  "@STRING{ file_title_tag.end = \"</title>\n\" }",
  "@STRING{ body_tag.start = \"<body bgcolor=\"#ffffff\" link=\"blue\" alink=\"blue\" vlink=\"blue\">\n\n\" }",
  "@STRING{ body_tag.end = \"\n\n</body>\n\n\" }",
  "@STRING{ page_title_tag.start = \"\n<table width=\"100%\">\n\
<tr><td height=\"50\" bgcolor=\"#669999\" align=\"center\">\n\
<strong><font size=6 color=\"#ffffff\" face=\"times\">\n\" }",
  "@STRING{ page_title_tag.end = \"\n</font></strong>\n</td></tr>\n</table>\n\n\" }",
  "@STRING{ page_subtitle_tag.start = \"\n<table width=\"100%\">\n\
<tr><td height=\"35\"  align=\"center\" valign=\"center\" bgcolor=\"#badfe1\">\n\
<strong><font size=4 face=\"times\">\n\" }",
  "@STRING{ page_subtitle_tag.end = \"\n</font></strong>\n</td></tr>\n</table>\n\n\" }",
  "@STRING{ list_tag.start = \"\n<ol>\n\" }",
  "@STRING{ list_tag.end = \"\n</ol>\n\" }",
  "@STRING{ item_tag.start = \"\n<li>\n\" }",
  "@STRING{ item_tag.end = \"\n</li>\n<br /><br />\n\n\" }",
  "@STRING{ separator = \"\n<br /><hr size=\"2\" width=\"100%\"><br />\n\n\" }",
  "@STRING{ line_break = \"\n<br />\n\" }",
  "@STRING{ disclaimer.prefix = \"<u><strong>Disclaimer:</strong></u><br /><br />\n\" }",
  "@STRING{ disclaimer.content.start = \"<p><em>\n\" }",
  "@STRING{ disclaimer.content.end = \"\n</em></p>\n\" }",
  "@STRING{ date_tag.start = \"Last modified: \" }",
  "@STRING{ date_tag.end = \"\n\" }",
  "@STRING{ author_tag.start = \"<br />Author: \" }",
  "@STRING{ author_tag.end = \".\n\" }",  
  "@STRING{ disclaimer_1 = \"\
This material is presented to ensure timely dissemination of\n\
scholarly and technical work. Copyright and all rights therein\n\
are retained by authors or by other copyright holders.\n\
All person copying this information are expected to adhere to\n\
the terms and constraints invoked by each author's copyright.\n\
In most cases, these works may not be reposted\n\
without the explicit permission of the copyright holder.\n\" }",
  "@STRING{ disclaimer_2 = \"\
Les documents contenus dans ces r�pertoires sont rendus disponibles\n\
par les auteurs qui y ont contribu� en vue d'assurer la diffusion\n\
� temps de travaux savants et techniques sur une base non-commerciale.\n\
Les droits de copie et autres droits sont gard�s par les auteurs\n\
et par les d�tenteurs du copyright, en d�pit du fait qu'ils pr�sentent\n\
ici leurs travaux sous forme �lectronique. Les personnes copiant ces\n\
informations doivent adh�rer aux termes et contraintes couverts par\n\
le copyright de chaque auteur. Ces travaux ne peuvent pas �tre\n\
rendus disponibles ailleurs sans la permission explicite du d�tenteur\n\
du copyright.\n\" }",
  "@STRING{ credits = \"\
<p>This document was translated from BibT<sub>E</sub>X by\n\
<a href=\"http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html\"><em>bibtex2html</em></a>\n</p>\n\" }",
  "XXX"
};


char *default_specific_file_description_html[] = 
{ "@STRING{ default_header_of_body = \"\n<br />\n\
<a href=\"../index.html\"><strong> BACK TO INDEX </strong></a>\n\
<br /><br />\n\n\" }",
  "@STRING{ default_footer_of_contents = \"\n<br />\n\
<a href=\"../index.html\"><strong> BACK TO INDEX </strong></a>\n\
<br /><br />\n\n\" }",
  "@STRING{ default_build_index = 1 }",
  "@STRING{ default_initials.table.start = \"\n\n<br /><table align=\"center\" cellpadding=\"4\" cellspacing=\"4\">\n\" }",
  "@STRING{ default_initials.table.end = \"</table><br />\n\n\" }",
  "@STRING{ default_initials.row.start = \"<tr align=\"center\">\n\" }",
  "@STRING{ default_initials.row.end = \"</tr>\n\" }",
  "@STRING{ default_initials.cell.start = \"<td bgcolor=\"#badfe1\">\" }",
  "@STRING{ default_initials.cell.end   = \"</td>\n\" }",
  "@STRING{ default_initials.empty_cell.start = \"<td>\" }",
  "@STRING{ default_initials.empty_cell.end   = \"</td>\n\" }",
  "@STRING{ default_index.row.start = \"<tr align=\"left\" valign=\"top\">\n\" }",
  "@STRING{ default_index.row.end = \"</tr>\n\" }",
  "@STRING{ default_index.cell.start = \"<td>\" }",
  "@STRING{ default_index.cell.end = \"</td>\n\" }",
  "@STRING{ default_index.empty_cell.start = \"<td>\" }",
  "@STRING{ default_index.empty_cell.end = \"</td>\n\" }",
  "@STRING{ default_index.extra_cell.start = \"<td bgcolor=\"#badfe1\"><strong>\" }",
  "@STRING{ default_index.extra_cell.end = \"</strong></td>\n\" }",
  "@STRING{ default_index.empty_extra_cell.start = \"<td><strong>\" }",
  "@STRING{ default_index.empty_extra_cell.end = \"</strong></td>\n\" }",

  "@STRING{ index_header_of_body = \"<br />\" }",
  "@STRING{ index_footer_of_contents = \" \" }",

  "@STRING{ single_output_header_of_body = \"<br />\" }",
  "@STRING{ single_output_footer_of_contents = \" \" }",

  "@STRING{ author_build_initials_index = 1 }",
  "@STRING{ author_index.table.start = \"<table align=\"center\" cellpadding=\"3\" cellspacing=\"1\">\n\" }",
  "@STRING{ author_index.table.end = \"</table>\n<br />\n\" }",
  "@STRING{ author_index.cell.start = \"<td>\" }",
  "@STRING{ author_index.cell.end = \"</td>\n\" }",
  "@STRING{ author_put_initials_in_index = 1 }",
  "@STRING{ author_index.arg2_in_cell.start = \"<strong>\" }",
  "@STRING{ author_index.arg2_in_cell.end = \"</strong>\" }",
  "@STRING{ author_anchor_prefix = \"AUTH\" }",

  "@STRING{ category_index.table.start = \"\n\n<br /><table align=\"center\" cellpadding=\"4\" cellspacing=\"2\">\n\" }",
  "@STRING{ category_index.table.end = \"</table><br />\n\n\" }",
  "@STRING{ category_index.cell.start = \"<td>\" }",
  "@STRING{ category_index.cell.end = \"</td>\n\" }",

  "@STRING{ keyword_build_initials_index = 1 }",
  "@STRING{ keyword_index.table.start = \"<table align=\"center\" cellpadding=\"3\" cellspacing=\"1\">\n\" }",
  "@STRING{ keyword_index.table.end = \"</table>\n<br />\n\" }",
  "@STRING{ keyword_index.cell.start = \"<td>\" }",
  "@STRING{ keyword_index.cell.end = \"</td>\n\" }",
  "@STRING{ keyword_put_initials_in_index = 1 }",
  "@STRING{ keyword_anchor_prefix = \"KEYW\" }",

  "@STRING{ reduced_year_index.table.start = \"\n\n<br /><table align=\"center\" cellpadding=\"4\" cellspacing=\"2\">\n\" }",
  "@STRING{ reduced_year_index.table.end = \"</table><br />\n\n\" }",

  "@STRING{ complete_year_index.table.start = \"\n\n<br /><table align=\"center\">\n\" }",
  "@STRING{ complete_year_index.table.end = \"</table><br />\n\n\" }",

  "@STRING{ complete_biblio_index.table.start = \"\n\n<br /><table align=\"center\" cellpadding=\"4\" cellspacing=\"2\">\n\" }",
  "@STRING{ complete_biblio_index.table.end = \"</table><br />\n\n\" }",
  "@STRING{ complete_biblio_anchor_prefix = \"BIBL\" }",
  "XXX"
};



char *default_generic_file_description_latex[] = 
{ "@STRING{ file_tag.start = \"\\documentclass{article}\n\\usepackage{fullpage}\n\" }",
  "@STRING{ file_title_tag.start = \"\n% \" }",
  "@STRING{ file_title_tag.end = \"\n\" }",
  "@STRING{ body_tag.start = \"\n\\begin{document}\n\" }",
  "@STRING{ body_tag.end = \"\n\\end{document}\n\" }",
  "@STRING{ page_title_tag.start = \"\n\\vspace*{20mm}\n\\begin{center}\n\\large\n\" }",
  "@STRING{ page_title_tag.end = \"\n\\end{center}\n\\vspace*{20mm}\n\n }",
  "@STRING{ page_subtitle_tag.start = \"\n\\section*{\" }",
  "@STRING{ page_subtitle_tag.end = \"}\n\" }",
  "@STRING{ list_tag.start = \"\n\\begin{enumerate}\n\" }",
  "@STRING{ list_tag.end = \"\n\\end{enumerate}\n\" }",
  "@STRING{ item_tag.start = \"\\item \" }",
  "@STRING{ item_tag.end = \"\n\n\" }",
  "@STRING{ separator = \"\n\n\\vspace*{5mm}\n\n\" }",
  "@STRING{ line_break = \"\\\\\" }",
  "@STRING{ disclaimer_tag.start = \"\\subsubsection*{Disclaimer}\" }",
  "@STRING{ date_tag.start = \"{\\bf Last modified: }\" }",
  "@STRING{ date_tag.end = \"\n\" }",
  "@STRING{ author_tag.start = \"{\\bf Author: }\" }",
  "@STRING{ author_tag.end = \".\n\" }",  
  "@STRING{ disclaimer_1 = \"\
This material is presented to ensure timely dissemination of\n\
scholarly and technical work. Copyright and all rights therein are retained by authors\n\
or by other copyright holders.\n\
All person copying this information are expected to adhere to the terms and constraints\n\
invoked by each author's copyright. In most cases, these works may not be reposted\n\
without the explicit permission of the copyright holder.\n\" }",
  "@STRING{ disclaimer_2 = \"\
Les documents contenus dans ces r�pertoires sont rendus disponibles par les auteurs\n\
qui y ont contribu� en vue d'assurer la diffusion � temps de travaux savants et techniques\n\
sur une base non-commerciale. Les droits de copie et autres droits sont gard�s par les\n\
auteurs et par les d�tenteurs du copyright, en d�pit du fait qu'ils pr�sentent ici leurs\n\
travaux sous forme �lectronique. Les personnes copiant ces informations doivent adh�rer aux\n\
termes et contraintes couverts par le copyright de chaque auteur. Ces travaux ne peuvent pas\n\
�tre rendus disponibles ailleurs sans la permission explicite du d�tenteur du copyright.\n\" }",
  "@STRING{ credits = \"\
Automatically generated by {\\em bibtex2html}\n\
({\tt http://www-sop.inria.fr/epidaure/personnel/malandain/codes/bibtex2html.html})\n\
written by Gregoire Malandain\n\" }",
  "XXX"
};


char *default_specific_file_description_latex[] = 
{ "@STRING{ index_create_pages = 0 }",
  "@STRING{ author_create_pages = 0 }",
  "@STRING{ category_create_pages = 0 }",
  "@STRING{ keyword_create_pages = 0 }",
  "@STRING{ reduced_year_create_pages = 0 }",
  "@STRING{ complete_year_create_pages = 0 }",
  "@STRING{ complete_biblio_create_pages = 1 }",
  "XXX"
};



char *default_generic_file_description_txt[] = 
{
  "XXX"
};



char *default_specific_file_description_txt[] = 
{
  "XXX"
};



char *default_generic_file_description_none[] = 
{
  "XXX"
};



char *default_specific_file_description_none[] = 
{
  "XXX"
};





/* description of the structures
 */
typeArgDescription generic_file_description[] = 
{ { "file_tag",               _TAG_,   NULL },
  { "head_tag",               _TAG_,   NULL },
  { "file_title_tag",         _TAG_,   NULL },
  { "body_tag",               _TAG_,   NULL },
  { "header_of_body",         _STRING_, NULL },
  { "footer_of_body",         _STRING_, NULL },
  { "header_of_contents",     _STRING_, NULL },
  { "footer_of_contents",     _STRING_, NULL },
  { "page_title_tag",         _TAG_,   NULL },
  { "page_subtitle_tag",      _TAG_,   NULL },
  { "list_tag",               _TAG_,   NULL },
  { "item_tag",               _TAG_,   NULL },
  { "line_break",             _STRING_, NULL },
  { "separator",              _STRING_, NULL },
  { "disclaimer",             _ENV_,   NULL },
  { "disclaimer_1", _STRING_, NULL },
  { "disclaimer_2", _STRING_, NULL },
  { "date_tag",               _TAG_,   NULL },
  { "author_tag",             _TAG_,   NULL },
  { "author_name",            _STRING_, NULL },
  { "credits_tag",            _TAG_,   NULL },
  { "credits",      _STRING_, NULL },
  { "XXX",                    _STRING_, NULL }
};

typeArgDescription specific_file_description[] = 
{ { "create_pages",          _INT_,   NULL },
  { "write_complete_suffix", _INT_,   NULL },

  { "file_title",            _STRING_, NULL },
  { "default_file_title",    _STRING_, NULL },
  { "page_title",            _STRING_, NULL },
  { "default_page_title",    _STRING_, NULL },

  { "write_local_menu",      _INT_,   NULL },
  { "header_of_body",        _STRING_, NULL },
  { "footer_of_body",        _STRING_, NULL },
  { "header_of_contents",        _STRING_, NULL },
  { "footer_of_contents",        _STRING_, NULL },

  { "build_initials_index",  _INT_,   NULL },
  { "initials",              _TABLE_, NULL },
  { "put_initials_in_index", _INT_,   NULL },
  { "build_index",           _INT_,   NULL },
  { "index",                 _TABLE_, NULL },

  { "write_anchor",          _INT_,   NULL },
  { "anchor_prefix",         _STRING_, NULL },

  { "write_disclaimer",      _INT_,   NULL },
  { "write_date",            _INT_,   NULL },
  { "write_author",          _INT_,   NULL },
  { "write_credits",         _INT_,   NULL },

  { "write_bibtex_abstract", _INT_,   NULL },
  { "write_bibtex_annote",   _INT_,   NULL },
  { "write_bibtex_comments", _INT_,   NULL },
  { "write_bibtex_key",      _INT_,   NULL },
  { "write_bibtex_file",     _INT_,   NULL },
  { "write_bibtex_entry",    _INT_,   NULL },
  { "XXX",                   _STRING_, NULL }
};



void init_generic_file_description( typeGenericFileDescription *g )
{
  init_tag_element( &(g->file_tag) );
  init_tag_element( &(g->head_tag) );
  init_tag_element( &(g->file_title_tag) );
  init_tag_element( &(g->body_tag) );

  g->header_of_body = NULL;
  g->footer_of_body = NULL;

  g->header_of_contents = NULL;
  g->footer_of_contents = NULL;

  init_tag_element( &(g->page_title_tag) );
  init_tag_element( &(g->page_subtitle_tag) );

  init_tag_element( &(g->list_tag) );
  init_tag_element( &(g->item_tag) );

  g->separator = NULL;
  g->line_break = NULL;
  
  init_env_element( &(g->disclaimer) );
  g->disclaimer_1 = NULL;
  g->disclaimer_2 = NULL;

  init_tag_element( &(g->date_tag) );

  init_tag_element( &(g->author_tag) );
  g->author_name = NULL;

  init_tag_element( &(g->credits_tag) );
  g->credits = NULL;

}



void free_generic_file_description( typeGenericFileDescription *g )
{
  free_tag_element( &(g->file_tag) );
  free_tag_element( &(g->head_tag) );
  free_tag_element( &(g->file_title_tag) );
  free_tag_element( &(g->body_tag) );

  if ( g->header_of_body != NULL ) FREE( g->header_of_body, "free_generic_file_description" );
  if ( g->footer_of_body != NULL ) FREE( g->footer_of_body, "free_generic_file_description" );

  if ( g->header_of_contents != NULL ) FREE( g->header_of_contents, "free_generic_file_description" );
  if ( g->footer_of_contents != NULL ) FREE( g->footer_of_contents, "free_generic_file_description" );

  free_tag_element( &(g->page_title_tag) );
  free_tag_element( &(g->page_subtitle_tag) );

  free_tag_element( &(g->list_tag) );
  free_tag_element( &(g->item_tag) );

  if ( g->separator != NULL )  FREE( g->separator, "free_generic_file_description" );
  if ( g->line_break != NULL ) FREE( g->line_break, "free_generic_file_description" );
  
  free_env_element( &(g->disclaimer) );
  if ( g->disclaimer_1 != NULL ) FREE( g->disclaimer_1, "free_generic_file_description" );
  if ( g->disclaimer_2 != NULL ) FREE( g->disclaimer_2, "free_generic_file_description" );

  free_tag_element( &(g->date_tag) );

  free_tag_element( &(g->author_tag) );
  if ( g->author_name != NULL ) FREE( g->author_name, "free_generic_file_description" );

  free_tag_element( &(g->credits_tag) );
  if ( g->credits != NULL ) FREE( g->credits, "free_generic_file_description" );

  init_generic_file_description( g );
}



void init_specific_file_description( typeSpecificFileDescription *s )
{
  s->create_pages = -1;
  s->write_complete_suffix = -1;

  s->file_title = NULL;
  s->default_file_title = NULL;

  s->page_title = NULL;
  s->default_page_title = NULL;

  s->write_local_menu = -1;

  s->header_of_body = NULL;
  s->footer_of_body = NULL;

  s->header_of_contents = NULL;
  s->footer_of_contents = NULL;

  s->build_initials_index = -1;
  init_table_element( &(s->initials) );

  s->put_initials_in_index = -1;

  s->build_index = -1;
  init_table_element( &(s->index) );

  s->write_anchor = -1;
  s->anchor_prefix = NULL;

  s->write_disclaimer = -1;
  s->write_date = -1;
  s->write_author = -1;
  s->write_credits = -1;

  s->write_bibtex_abstract = -1;
  s->write_bibtex_annote = -1;
  s->write_bibtex_comments = -1;
  s->write_bibtex_key = -1;
  s->write_bibtex_file = -1;
  s->write_bibtex_entry = -1;
}



void free_specific_file_description( typeSpecificFileDescription *s )
{
  if ( s->file_title != NULL ) FREE( s->file_title, "free_specific_file_description" );
  if ( s->default_file_title != NULL ) FREE( s->default_file_title, "free_specific_file_description" );

  if ( s->page_title != NULL ) FREE( s->page_title, "free_specific_file_description" );
  if ( s->default_page_title != NULL ) FREE( s->default_page_title, "free_specific_file_description" );

  if ( s->header_of_body != NULL ) FREE( s->header_of_body, "free_specific_file_description" );
  if ( s->footer_of_body != NULL ) FREE( s->footer_of_body, "free_specific_file_description" );

  if ( s->header_of_contents != NULL ) FREE( s->header_of_contents, "free_specific_file_description" );
  if ( s->footer_of_contents != NULL ) FREE( s->footer_of_contents, "free_specific_file_description" );

  free_table_element( &(s->initials) );

  free_table_element( &(s->index) );

  if ( s->anchor_prefix != NULL ) FREE( s->anchor_prefix, "free_specific_file_description" );

  init_specific_file_description( s );
}





/* add file strings to customization strings
 */

int add_generic_file_strings( typeArgDescription *custom,
			      int length,
			      typeGenericFileDescription *f,
			      typeArgDescription *fd )
{
  char *proc = "add_generic_file_strings";
  int l = length;
  int i, j, n = 0;
  
  for ( i = 0, j = 0; strncmp( fd[i].key, "XXX", 3 ) != 0; i ++, j += n ) {

    n = 0;
    
    switch( fd[i].type ) {
      
    default :
      fprintf( stderr, "%s: unknown arg type\n", proc );
      break;

    case _STRING_ :

      custom[l+j]     = fd[i];
      if ( strcmp( fd[i].key, "header_of_body" ) == 0 )
	custom[l+j].arg = &(f->header_of_body);
      else if ( strcmp( fd[i].key, "footer_of_body" ) == 0 )
	custom[l+j].arg = &(f->footer_of_body);

      else if ( strcmp( fd[i].key, "header_of_contents" ) == 0 )
	custom[l+j].arg = &(f->header_of_contents);
      else if ( strcmp( fd[i].key, "footer_of_contents" ) == 0 )
	custom[l+j].arg = &(f->footer_of_contents);

      else if ( strcmp( fd[i].key, "line_break" ) == 0 )
	custom[l+j].arg = &(f->line_break);
      else if ( strcmp( fd[i].key, "separator" ) == 0 )
	custom[l+j].arg = &(f->separator);

      else if ( strcmp( fd[i].key, "disclaimer_1" ) == 0 )
	custom[l+j].arg = &(f->disclaimer_1);
      else if ( strcmp( fd[i].key, "disclaimer_2" ) == 0 )
	custom[l+j].arg = &(f->disclaimer_2);
      else if ( strcmp( fd[i].key, "author_name" ) == 0 )
	custom[l+j].arg = &(f->author_name);
      else if ( strcmp( fd[i].key, "credits" ) == 0 )
	custom[l+j].arg = &(f->credits);
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, fd[i].key );
      }     
      n = 1;
      break;

    case _TAG_ :

      if ( strcmp( fd[i].key, "file_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->file_tag) );
      else if ( strcmp( fd[i].key, "head_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->head_tag) );
      else if ( strcmp( fd[i].key, "file_title_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->file_title_tag) );
      else if ( strcmp( fd[i].key, "body_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->body_tag) );
      
      else if ( strcmp( fd[i].key, "page_title_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->page_title_tag) );
      else if ( strcmp( fd[i].key, "page_subtitle_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->page_subtitle_tag) );
      
      else if ( strcmp( fd[i].key, "list_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->list_tag) );
      else if ( strcmp( fd[i].key, "item_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->item_tag) );
      
      else if ( strcmp( fd[i].key, "date_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->date_tag) );
      else if ( strcmp( fd[i].key, "author_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->author_tag) );
      else if ( strcmp( fd[i].key, "credits_tag" ) == 0 )
	n = add_tag_element( custom, l+j, fd[i].key, &(f->credits_tag) );

      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, fd[i].key );
      }      
      break;
      
    case _ENV_ :

      if ( strcmp( fd[i].key, "disclaimer" ) == 0 )
	n = add_env_element( custom, l+j, fd[i].key, &(f->disclaimer) );
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, fd[i].key );
      }      
      break;

    }
  }
  return ( l+j );
}



int add_specific_file_strings( typeArgDescription *custom,
			       int length,
			       char *prefix,
			       typeSpecificFileDescription *f,
			       typeArgDescription *fd )
{
  char *proc = "add_specific_file_strings";
  int l = length;
  int i, j, n = 0;
  char *str = NULL;

  str = (char*)MALLOC( SHORT_STR_LENGTH, "add_specific_file_strings" );
  if ( str == NULL ) return( -1 );

  for ( i = 0, j = 0; strncmp( fd[i].key, "XXX", 3 ) != 0; i ++, j += n ) {

    n = 0;

    switch( fd[i].type ) {
      
    default :
      fprintf( stderr, "%s: unknown arg type\n", proc );
      break;

    case _STRING_ :

      custom[l+j]     = fd[i];
      if ( prefix != NULL && prefix[0] != '\0' )
	sprintf( custom[l+j].key, "%s_%s", prefix, fd[i].key );

      if ( strcmp( fd[i].key, "file_title" ) == 0 )
	custom[l+j].arg = &(f->file_title);
      else if ( strcmp( fd[i].key, "default_file_title" ) == 0 )
	custom[l+j].arg = &(f->default_file_title);
      else if ( strcmp( fd[i].key, "page_title" ) == 0 )
	custom[l+j].arg = &(f->page_title);
      else if ( strcmp( fd[i].key, "default_page_title" ) == 0 )
	custom[l+j].arg = &(f->default_page_title);

      else if ( strcmp( fd[i].key, "header_of_body" ) == 0 )
	custom[l+j].arg = &(f->header_of_body);
      else if ( strcmp( fd[i].key, "footer_of_body" ) == 0 )
	custom[l+j].arg = &(f->footer_of_body);
      else if ( strcmp( fd[i].key, "header_of_contents" ) == 0 )
	custom[l+j].arg = &(f->header_of_contents);
      else if ( strcmp( fd[i].key, "footer_of_contents" ) == 0 )
	custom[l+j].arg = &(f->footer_of_contents);

      else if ( strcmp( fd[i].key, "anchor_prefix" ) == 0 )
	custom[l+j].arg = &(f->anchor_prefix);
      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, fd[i].key );
      }     
      n = 1;
      break;
      
    case _INT_ :
      
      custom[l+j]     = fd[i];
      if ( prefix != NULL && prefix[0] != '\0' )
	sprintf( custom[l+j].key, "%s_%s", prefix, fd[i].key );

      if ( strcmp( fd[i].key, "create_pages" ) == 0 )
	custom[l+j].arg = &(f->create_pages);
      else if ( strcmp( fd[i].key, "write_complete_suffix" ) == 0 )
	custom[l+j].arg = &(f->write_complete_suffix);
      else if ( strcmp( fd[i].key, "write_local_menu" ) == 0 )
	custom[l+j].arg = &(f->write_local_menu);

      else if ( strcmp( fd[i].key, "build_initials_index" ) == 0 )
	custom[l+j].arg = &(f->build_initials_index);
      else if ( strcmp( fd[i].key, "put_initials_in_index" ) == 0 )
	custom[l+j].arg = &(f->put_initials_in_index);
      else if ( strcmp( fd[i].key, "build_index" ) == 0 )
	custom[l+j].arg = &(f->build_index);

      else if ( strcmp( fd[i].key, "write_anchor" ) == 0 )
	custom[l+j].arg = &(f->write_anchor);

      else if ( strcmp( fd[i].key, "write_disclaimer" ) == 0 )
	custom[l+j].arg = &(f->write_disclaimer);
      else if ( strcmp( fd[i].key, "write_date" ) == 0 )
	custom[l+j].arg = &(f->write_date);
      else if ( strcmp( fd[i].key, "write_author" ) == 0 )
	custom[l+j].arg = &(f->write_author);
      else if ( strcmp( fd[i].key, "write_credits" ) == 0 )
	custom[l+j].arg = &(f->write_credits);
    
      else if ( strcmp( fd[i].key, "write_bibtex_abstract" ) == 0 )
	custom[l+j].arg = &(f->write_bibtex_abstract);
      else if ( strcmp( fd[i].key, "write_bibtex_annote" ) == 0 )
	custom[l+j].arg = &(f->write_bibtex_annote);
      else if ( strcmp( fd[i].key, "write_bibtex_comments" ) == 0 )
	custom[l+j].arg = &(f->write_bibtex_comments);
      else if ( strcmp( fd[i].key, "write_bibtex_key" ) == 0 )
	custom[l+j].arg = &(f->write_bibtex_key);
      else if ( strcmp( fd[i].key, "write_bibtex_file" ) == 0 )
	custom[l+j].arg = &(f->write_bibtex_file);
      else if ( strcmp( fd[i].key, "write_bibtex_entry" ) == 0 )
	custom[l+j].arg = &(f->write_bibtex_entry);

      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, fd[i].key );
      }
      n = 1;
      break;

    case _TABLE_ :

      if ( prefix != NULL && prefix[0] != '\0' )
	sprintf( str, "%s_%s", prefix, fd[i].key );
      else
	sprintf( str, fd[i].key );

      if ( strcmp( fd[i].key, "initials" ) == 0 )
	n = add_table_element( custom, l+j, str, &(f->initials) );
      else if ( strcmp( fd[i].key, "index" ) == 0 )
	n = add_table_element( custom, l+j, str, &(f->index) );

      else {
	custom[l+j].arg = NULL;
	fprintf( stderr, "%s: unable to link key '%s' with arg\n",
		 proc, fd[i].key );
      }
      break;
   
    }
  }
  FREE( str, "add_specific_file_strings" );
  return( l+j );
}










/**************************************************
 *
 * Update of specific files with default
 *
 **************************************************/
						   

int update_string( char **f, char *d, int force_update )
{
  int l, length;

  l = 0;
  if ( d == NULL || d[0] == '\0' ) {
    if ( *f != NULL && force_update ) {
      FREE( *f, "update_string" );
      *f = NULL;
    }
    return( 1 );
  }
  if ( *f != NULL && (*f)[0] != '\0' && !force_update )
    return( 1 );

  l = strlen( d );
  length = 0;
  if ( *f != NULL && (*f)[0] != '\0' ) length = strlen( *f );

  if ( length == l && strcmp( *f, d ) == 0 ) {
    return( 1 );
  }

  if ( length < l+1 ) {
    if ( *f != NULL ) {
      FREE( *f, "update_string" );
      *f = NULL;
    }
    *f = (char*)MALLOC( l+1, "update_string" );
    if ( *f == NULL ) return( 0 );
  }
  (void)memcpy( *f, d, l );
  (*f)[l] = '\0';
  return( 1 );
}



int update_element( typeTagElement *f, typeTagElement *d, int force_update )
{
  if ( update_string( &(f->start), (d->start), force_update ) != 1 ) return( 0 );
  if ( update_string( &(f->end),   (d->end)  , force_update ) != 1 ) return( 0 );
  return( 1 );
}



int update_table( typeTableElement *f, typeTableElement *d, int force_update )
{
  if ( update_element( &(f->table), &(d->table), force_update ) != 1 ) return( 0 );
  if ( update_element( &(f->row),   &(d->row)  , force_update ) != 1 ) return( 0 );
  if ( update_element( &(f->cell),  &(d->cell) , force_update ) != 1 ) return( 0 );
  if ( update_element( &(f->empty_cell),  &(d->empty_cell), force_update  ) != 1 ) return( 0 );
  if ( update_element( &(f->arg1_in_cell), &(d->arg1_in_cell), force_update ) != 1 ) return( 0 );
  if ( update_element( &(f->arg2_in_cell), &(d->arg2_in_cell), force_update ) != 1 ) return( 0 );

  if ( d->cells_per_row != -1 ) f->cells_per_row = d->cells_per_row;
  if ( update_element( &(f->extra_cell),   &(d->extra_cell), force_update   ) != 1 ) return( 0 );
  if ( update_element( &(f->empty_extra_cell),   &(d->empty_extra_cell), force_update   ) != 1 ) return( 0 );
  return( 1 );
}



int update_file_description_with_default ( typeSpecificFileDescription *f,
					   typeSpecificFileDescription *d )
{
  int force_update = 0;

  if ( d->create_pages != -1 ) 
    f->create_pages = d->create_pages;
  if ( d->write_complete_suffix != -1 )    
    f->write_complete_suffix = d->write_complete_suffix;

  if ( update_string( &(f->file_title), 
		       (d->file_title), force_update ) != 1 ) return( 0 );
  if ( update_string( &(f->default_file_title), 
		       (d->default_file_title), force_update ) != 1 ) return( 0 );
  if ( update_string( &(f->page_title), 
		       (d->page_title), force_update ) != 1 ) return( 0 );
  if ( update_string( &(f->default_page_title), 
		       (d->default_page_title), force_update ) != 1 ) return( 0 );

  if ( d->write_local_menu != -1 ) f->write_local_menu = d->write_local_menu;
  
  if ( update_string( &(f->header_of_body), 
		       (d->header_of_body), force_update ) != 1 ) return( 0 );
  if ( update_string( &(f->footer_of_body), 
		       (d->footer_of_body), force_update ) != 1 ) return( 0 );

  if ( update_string( &(f->header_of_contents), 
		       (d->header_of_contents), force_update ) != 1 ) return( 0 );
  if ( update_string( &(f->footer_of_contents), 
		       (d->footer_of_contents), force_update ) != 1 ) return( 0 );

  if ( d->build_initials_index != -1 ) 
    f->build_initials_index = d->build_initials_index;

  if (  update_table( &(f->initials),
		      &(d->initials), force_update ) != 1 ) return( 0 );
  
  if ( d->put_initials_in_index != -1 ) 
    f->put_initials_in_index = d->put_initials_in_index;

  if ( d->build_index != -1 ) 
    f->build_index = d->build_index;
  if (  update_table( &(f->index),
		      &(d->index), force_update ) != 1 ) return( 0 );
  
  if ( d->write_anchor != -1 )     
    f->write_anchor = d->write_anchor;
  if ( update_string( &(f->anchor_prefix), 
		       (d->anchor_prefix), force_update ) != 1 ) return( 0 );

  if ( d->write_disclaimer != -1 ) f->write_disclaimer = d->write_disclaimer;
  if ( d->write_date != -1 )       f->write_date       = d->write_date;
  if ( d->write_author != -1 )     f->write_author     = d->write_author;
  if ( d->write_credits != -1 )    f->write_credits    = d->write_credits;

  if ( d->write_bibtex_abstract != -1 ) 
    f->write_bibtex_abstract = d->write_bibtex_abstract;
  if ( d->write_bibtex_annote != -1 )   
    f->write_bibtex_annote   = d->write_bibtex_annote;
  if ( d->write_bibtex_comments != -1 ) 
    f->write_bibtex_comments = d->write_bibtex_comments;
  if ( d->write_bibtex_key != -1 )      
    f->write_bibtex_key      = d->write_bibtex_key;
  if ( d->write_bibtex_file != -1 )      
    f->write_bibtex_file      = d->write_bibtex_file;
  if ( d->write_bibtex_entry != -1 )    
    f->write_bibtex_entry    = d->write_bibtex_entry;


  return( 1 );
}
